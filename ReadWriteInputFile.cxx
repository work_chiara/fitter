#include "ReadWriteInputFile.h"
#include <iostream>
#include <fstream>


using namespace std;

ClassImp(ReadWriteInputFile)

ReadWriteInputFile::ReadWriteInputFile(string inputFilePath) : TObject() {
    _filePath = inputFilePath;

}

map<string, string> *ReadWriteInputFile::readString() {
    string paramName;
    string paramValue;
    map<string, string> *valueInputMap = new map<string, string>();
    ifstream inputFile(_filePath); // cambiare il path
    if (inputFile.is_open()) {
      string line;
      string delimiter = " ";
      while (getline(inputFile, line)) {
        if (!line.empty() && line.find("#") == string::npos) {
            paramName = line.substr(0, line.find_first_of(delimiter));
            paramValue = line.substr(line.find_first_of(delimiter)+delimiter.length(), line.length());
            valueInputMap->insert(pair<string, string>(paramName, paramValue));
        }
      }

      if (inputFile.bad()) {
        cerr << "Unable to open init file " << _filePath <<
             endl;
      } else if (!inputFile.eof()) {
        cerr << "Format error. Unable to read correctly the configuration file " <<endl;
      } else {
        inputFile.close();
      }

    } else
        cout << "Unable to open init file " << _filePath <<
             endl;
    return
            valueInputMap;

}

map<string, double> *ReadWriteInputFile::read() {
    string paramName;
    double paramValue;
    map<string, double> *valueInputMap = new map<string, double>();
    ifstream inputFile(_filePath); // cambiare il path
    if (inputFile.is_open()) {
        while (inputFile >> paramName >> paramValue) {
            if (paramName.find("#") == string::npos) {
                valueInputMap->insert(pair<string, double>(paramName, paramValue));
            }
        }
        inputFile.close();
    } else cout << "Unable to open init file " << _filePath << endl;
    return valueInputMap;

}

void ReadWriteInputFile::write(map<string, double> *parameterMap) {
    ofstream outFile(_filePath);
    for (auto &elMap : *parameterMap) {
        outFile << elMap.first << " " << elMap.second << endl;
    }
    outFile.close();
}
