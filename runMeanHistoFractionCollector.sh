#!/bin/bash

source /Applications/root_v6.08.02/bin/thisroot.sh

path=toyMC/**/**/fsigHisto.root

for f in $path
do
file=$f
IFS='/'
read -a array2 <<< "$f"
numSig=${array2[1]//sig_}
numBkg=${array2[2]//bkg_}
echo $numSig
echo $numBkg
unset IFS

/Applications/root_v6.08.02/bin/root -l -q -b toyMC/extractMeanSigma.cpp\(\"$file\",\"$numSig\",\"$numBkg\"\)
done
