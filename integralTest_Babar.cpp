#include "RooRealVar.h"
#include "RooGaussian.h"
#include "RooConstVar.h"
#include "RooAbsReal.h"
#include "RooPlot.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "RooEffProd.h"

using namespace RooFit;

void integralTest_Babar() {

    // create observable
    RooRealVar B__deltaE_corr("B__deltaE_corr", "B__deltaE_corr", -0.1, 0.1);

    // create pdf
    a0 = new RooRealVar("a0_deltaE_bkg", "first parameter", -4, 4);
    a1 = new RooRealVar("a1_deltaE_bkg", "second parameter", -6, 6);

    a0->setVal(-1.086);
    a0->setConstant();
    a1->setVal(-1.82);
    a1->setConstant();

    mean1 = new RooRealVar("mean1", "mean of the first gaussian for deltaE variable", -0.1, 0.1);
    sigma1 = new RooRealVar("sigma1", "width of the first gaussian for deltaE variable", 0.01, 0.02);
    mean1->setVal(0);
    mean1->setConstant();
    sigma1->setVal(1.0333e-02);
    sigma1->setConstant();


    RooArgList *polyParamList = new RooArgList(*a0, *a1);
    RooAbsPdf *pol2 = new RooPolynomial("pol2", "gaussian PDF for deltaE variable", B__deltaE_corr, *polyParamList);
    //RooAbsPdf *pol1 = new RooPolynomial("pol1", "pol1", B__deltaE_corr, *a0);
    //gaussian1 = new RooGaussian("gaussian1","gaussian 1 PDF for deltaE variable",B__deltaE_corr,*mean1,*sigma1);

    //RooGenericPdf genPdf("genPdf","(1-1.7985*B__deltaE_corr+5.1165*B__deltaE_corr*B__deltaE_corr)",B__deltaE_corr);

    // Return 'raw' unnormalized value of gx
    cout << "pol2 = " << pol2->getVal() << endl;
    //cout << "genPdf = " << genPdf.getVal() << endl ;
    //cout << "gaussian1 = " << gaussian1->getVal() << endl ;

    //RooAbsReal * fracIntGauss1 = gaussian1->createIntegral(B__deltaE_corr);
    //cout << "pre fracIntGauss1 = " << fracIntGauss1->getVal() << endl ;

    B__deltaE_corr.setRange("signal", -0.045, 0.045);

    RooAbsReal *fracInt = pol2->createIntegral(B__deltaE_corr, NormSet(B__deltaE_corr), Range("signal"));
    cout << "fracInt = " << fracInt->getVal() << endl;


}
