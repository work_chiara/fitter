#ifndef Observables_NEW
#define Observables_NEW

static const char *const B__DELTAE_CORR = "B__deltaE_corr";

static const char *const B__HELK = "B__helK";

static const char *const B__HELR = "B__helR";

static const char *const B__MR = "B__MR";

static const char *const B__MK = "B__MK";

static const char *const B__MBC_CORR = "B__Mbc_corr";

#include "RooRealVar.h"
#include "RooArgList.h"
#include <string>
#include <vector>

#ifndef ROOT_TObject
#include "TObject.h"
#endif

using namespace std;
using namespace RooFit;

class Observables : public TObject {

    RooArgList observableList;

public:
    Observables();

    RooRealVar *getObeservable(string nameObservable);

ClassDef(Observables, 1);

};

#endif
