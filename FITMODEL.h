#ifndef FITMODEL_NEW
#define FITMODEL_NEW

static const char *const SIGNAL = "signal";

static const char *const UDS_BKG = "uds_bkg";

static const char *const CHARM_BKG = "charm_bkg";

static const char *const CONTROL_CHANNEL = "controlChannel";

#include "pdf/background1/global/GlobalPdfBackground1.h"
#include "pdf/charm_bkg/global/GlobalPdfCharmBkg.h"
#include "pdf/signal/global/GlobalPdfSignal.h"
#include "pdf/controlChannel/global/GlobalPdfControlChannel.h"

#include "RooAddPdf.h"
#include "RooRealVar.h"
#include "RooArgList.h"
#include "Observables.h"
#include <fstream>

#ifndef ROOT_TObject
#include "TObject.h"
#endif

class FITMODEL : public TObject {

    GlobalPdfBackground1 *globalBackground_uds;
    GlobalPdfCharmBkg *globalBackground_charm;
    GlobalPdfSignal *globalSignal;
    GlobalPdfControlChannel *globalControlChannel;
    RooArgList *pdfCategoryList;
    RooArgList *yieldList;
    RooAddPdf *finalPdf;
    Observables *observable_obj;
    ofstream *_myLogger;

public:

    FITMODEL(bool initParamater, ofstream *myLogger);

    ~FITMODEL();

    RooDataSet *genetateData(int numberEvents, RooAbsPdf *finalPdf);

    RooAbsPdf *getPdfSingleObservable(string category, string nameObservable, string signalPolarization = "");

    RooRealVar *getObservable(string nameObservable);

    RooAbsPdf *getPdf_ForTest_fixedParam(string nameObservable);

    RooAbsPdf *get_6observable_Signal_ForTest();

    RooAbsPdf *get_6observable_Signal_And_Background_ForTest(double d, double d1);

    RooAbsPdf *get_6observable_Signal_And_Background_ForTest_fixed_fraction();

    RooAbsPdf *get_6observable_Signal_ForTest_fixed_fraction();

    RooAbsPdf *finalModel_ForTest();

ClassDef(FITMODEL, 2);

};

#endif
