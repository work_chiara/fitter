#include "Observables.h"

using namespace std;
using namespace RooFit;

ClassImp(Observables)

Observables::Observables() : TObject() {


    RooRealVar B__deltaE_corr(B__DELTAE_CORR, B__DELTAE_CORR, -0.02, 0.02);
    RooRealVar B__Mbc_corr(B__MBC_CORR, B__MBC_CORR, 5.255, 5.29);
    RooRealVar B__MK_var(B__MK, B__MK, 0.792, 0.992);
    RooRealVar B__MR_var(B__MR, B__MR, 0.52, 1.05);
    RooRealVar B__helK(B__HELK, B__HELK, -1, 1);
    RooRealVar B__helR(B__HELR, B__HELR, -1, 1);
    observableList.addClone(B__deltaE_corr);
    observableList.addClone(B__Mbc_corr);
    observableList.addClone(B__MK_var);
    observableList.addClone(B__MR_var);
    observableList.addClone(B__helK);
    observableList.addClone(B__helR);

}

RooRealVar *Observables::getObeservable(string nameObservable) {

    char *cstr = new char[nameObservable.length() + 1];
    std::strcpy(cstr, nameObservable.c_str());
    RooRealVar *var = (RooRealVar *) (observableList.find(cstr));
    return var;

}
