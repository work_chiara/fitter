// code to read file with workspace
// g++ `root-config --cflags --glibs` -L $ROOTSYS/lib -L $ROOTSYS/include -lRooFitCore -lRooFit -o fileTest readFileTest.cpp
// then ./fileTest
#include <THStack.h>
#include <TText.h>
#include "RooDataSet.h"
#include "TApplication.h"
#include "RooWorkspace.h"
#include "TFile.h"
#include "RooRealVar.h"
#include "RooGaussian.h"
#include "TCanvas.h"
#include "RooPlot.h"
#include "RooFitResult.h"

void reduceDataset(RooAbsData *data, RooRealVar *Mrho, const RooRealVar *Mbc);

void printValues(const RooAbsData *data);

void printMBCValues(const RooAbsData *data);

void createEmptyDatasetAndFill(RooAbsData *data, RooRealVar *Mrho, const RooRealVar *Mbc, const RooRealVar *deltaE);

void histo(RooAbsData *data);

void mergeHelicity(RooAbsData *d1, RooAbsData *d2);

using namespace RooFit;
using namespace std;

int main(int argc, char **argv) {
    TApplication theApp("App", &argc, argv);

    // Open input file with workspace
    //TFile *fGen = new TFile("tot_Bu_Rho0Kst+_K0spi+_Longitudinal.root");
    //TFile *f = new TFile("K0spi_Bu_Rho0Kst+_K0spi+_Longitudinal_tot.root");
    //TFile *f = new TFile("tot_st0_CharmOnResonance_K0spi_B2KstRho.root");
    //TFile *f = new TFile("fileAfterPre_selection/tot_Bu_Rho0Kst+_K0spi+_Longitudinal_B2KstRho.root");
    TFile *f = new TFile("fileControlChannel/tot_Bu_JPsiKst+_K0spi+_Transverse_B2KstRho.root");
    //TFile *f = new TFile("fileControlChannel/tot_Bu_JPsiKst+_K0spi+_Longitudinal_B2KstRho.root");
    //TFile *f = new TFile("K0spi_UDSMC_tot.root");
    //TFile *f = new TFile("fileAfterPre_selection/tot_st1_UDSOnResonance_K0spi+_K0spi_B2KstRho.root");
    //RooWorkspace *wGen = (RooWorkspace *) fGen->Get("wspace");
    //wGen->Print();

    RooWorkspace *w = (RooWorkspace *) f->Get("wspace");
    w->Print();

    //std::list<RooAbsData*> pippo = w->allData();
    //cout << "lenght pippo = " << pippo.size() << endl;

    RooAbsData *data_transverse = w->data("dataset");

    //RooRealVar *Mrho = w->var(B__MR);
    //RooRealVar *Mrho = w->var("Mrho");
    //RooRealVar *deltaE = w->var("B__deltaE_corr");
    //RooRealVar *Mbc = w->var(B__MBC_CORR); //ok for sig
    //RooRealVar *Mbc = w->var("Mbc");

    //cout << "numero di entries = " << data_transverse->numEntries() << endl;
    //cout << "numero di entries = " << data_long->numEntries() << endl;

    // Stampo Lower/Upper e Default value
    //cout << "Il valore di Mrho e' " << Mrho->getVal() << endl;
    //if (Mrho->hasMin()) {
    //    cout << "Mrho Min: " << Mrho->getMin() << endl;
    //cout << "Mrho Min: " << Mrho->getMax() << endl;
    //}

    // Creo Istogramma
    TH1 *dh2 = data_transverse->createHistogram(B__MR);
    dh2->Draw();

    // Graph with all values of MBC
    //printMBCValues(data);

    // Test Print 1000 Values from Data
    //printValues(data);

    // Test histogram
    //histo(data_long);
    // Test Reduce Dataset
    //reduceDataset(data, Mrho, Mbc);

    // Create Empty Dataset and fill with one row
    //createEmptyDatasetAndFill(data, Mrho, Mbc, deltaE);

    //mergeHelicity(data_transverse,data_long);

    theApp.Run();
}

void histo(RooAbsData *data) {

    for (int i = 0; i < 2; ++i) {
        const RooArgSet *argSet = data->get(i); // Indice < nEntries
        //    argSet->writeToFile("dati.txt"); // Scrive la var su file

        RooRealVar *Mbc = (RooRealVar *) argSet->find(B__MR);
        cout << "Il valore di Mbc e' " << Mbc->getValV() << endl;
    }


    TH1 *dh2 = data->createHistogram(B__MBC_CORR);
    //TH1 *dh2 = data->createHistogram(B__HELR);
    //dh2->SetMinimum(0);
    //dh2->SetMaximum(4);
    dh2->SetMarkerColor(2);
    dh2->SetMarkerStyle(25);
    cout << "integral = " << dh2->Integral() << endl;

    TCanvas *cs = new TCanvas("cs", "cs", 10, 10, 700, 500);
    TText T;
    T.SetTextFont(42);
    T.SetTextAlign(21);
    cs->cd();
    dh2->Draw();


}

void createEmptyDatasetAndFill(RooAbsData *data, RooRealVar *Mrho, const RooRealVar *Mbc, const RooRealVar *deltaE) {
    cout << "----------------------- CREO UN DATASET VUOTO E CI METTO due RIGHE -----------------------" << endl;

    RooAbsData *newDataset = data->emptyClone("clonato"); // Clona il Dataset ma vuoto

    RooArgSet *row = new RooArgSet(RooArgList(*Mbc, *Mrho, *deltaE));
    row->setRealValue("Mbc", 5.28);
    row->setRealValue("Mrho", 0.5);
    row->setRealValue("deltaE", 0.1);

    newDataset->add(*row); // Aggiungo la riga

    RooArgSet *row2 = new RooArgSet(RooArgList(*Mbc, *Mrho, *deltaE));
    row2->setRealValue("Mbc", 5.26);
    row2->setRealValue("Mrho", 1);
    row2->setRealValue("deltaE", 0.0);
    newDataset->add(*row2); // Aggiungo la riga

    cout << "numero di entries = " << newDataset->numEntries() << endl;

    for (int i = 0; i < 2; ++i) {
        const RooArgSet *argSet = newDataset->get(i); // Indice < nEntries
        //    argSet->writeToFile("dati.txt"); // Scrive la var su file

        RooRealVar *Mbc = (RooRealVar *) argSet->find("Mbc");
        RooRealVar *Mrho = (RooRealVar *) argSet->find("Mrho");
        cout << "Il valore di Mrho e' " << Mrho->getValV() << endl;
        cout << "Il valore di Mbc e' " << Mbc->getValV() << endl;
    }

    // Creo Istogramma
    TH1 *dh1 = newDataset->createHistogram("Mrho");
    dh1->SetMinimum(0);
    dh1->SetMaximum(4);
    dh1->SetMarkerColor(2);
    dh1->SetMarkerStyle(25);
//    dh1->Draw();

    TH1 *dh2 = newDataset->createHistogram("Mbc");
    dh2->SetMinimum(0);
    dh2->SetMaximum(4);
    dh2->SetMarkerColor(2);
    dh2->SetMarkerStyle(25);
//    dh2->Draw();
    TCanvas *cs = new TCanvas("cs", "cs", 10, 10, 700, 500);
    TText T;
    T.SetTextFont(42);
    T.SetTextAlign(21);
    cs->Divide(2, 1);
    cs->cd(1);
    dh1->Draw();
    cs->cd(2);
    dh2->Draw();
//    printMBCValues(newDataset); // Decommenta per printare il plot
}

void printValues(const RooAbsData *data) {// Read a Value

    cout << "----------------------- STAMPO 100 VALORI -----------------------" << endl;

    for (int i = 0; i < 100; ++i) {
        const RooArgSet *argSet = data->get(i); // Indice < nEntries
        //    argSet->writeToFile("dati.txt"); // Scrive la var su file

        RooRealVar *Mbc = (RooRealVar *) argSet->find(B__MBC_CORR); // ok for signal
        //RooRealVar *Mbc = (RooRealVar *) argSet->find("Mbc");
        //RooRealVar *Mrho = (RooRealVar *) argSet->find("Mrho");
        //cout << "Il valore di Mrho e' " << Mrho->getValV() << endl;
        cout << "Il valore di Mbc e' " << Mbc->getValV() << endl;
    }
}

void printMBCValues(const RooAbsData *data) {// Read a Value
    cout << "----------------------- STAMPO IL PLOT DELLA VAR MBC -----------------------" << endl;

    const RooArgSet *argSet = data->get(0); // Indice < nEntries
    RooRealVar *Mbc = (RooRealVar *) argSet->find("Mbc");
    RooPlot *frame = Mbc->frame();
    data->plotOn(frame);
    frame->Draw();
}

void
reduceDataset(RooAbsData *data, RooRealVar *Mrho, const RooRealVar *Mbc) {// Come ridurre il dataset con una formula
    cout << "----------------------- RIDUCO IL DATASET CON UNA FORMULA -----------------------" << endl;

    // Prendo da data tutte le righe in cui Mrho è 0.775
    RooFormulaVar formulaVar("formula", "Mrho==0.775", RooArgList(*Mbc, *Mrho));
    RooAbsData *reducedData = data->reduce(formulaVar);

    cout << "reducedData numero di entries = " << reducedData->numEntries() << endl;

    // Read a Value
    const RooArgSet *reducedDataargSet = reducedData->get(0); // Indice < nEntries
    Mrho = (RooRealVar *) reducedDataargSet->find("Mrho");
    cout << "Il valore di Mrho e' " << Mrho->getVal() << endl;
}

void mergeHelicity(RooAbsData *d1, RooAbsData *d2) {
    RooAbsData *newDataset = d1->emptyClone("clone");
    for (int i = 0; i < d1->numEntries(); ++i) {
        const RooArgSet *argSet = d1->get(i);
        newDataset->add(*argSet);
    }
    for (int i = 0; i < d2->numEntries(); ++i) {
        const RooArgSet *argSet = d2->get(i);
        newDataset->add(*argSet);
    }
    newDataset->Print();

    TH1 *dh2 = newDataset->createHistogram(B__HELR);
    //dh2->SetMinimum(0);
    //dh2->SetMaximum(4);
    dh2->SetMarkerColor(2);
    dh2->SetMarkerStyle(25);
    cout << "integral = " << dh2->Integral() << endl;

    TCanvas *cs = new TCanvas("cs", "cs", 10, 10, 700, 500);
    TText T;
    T.SetTextFont(42);
    T.SetTextAlign(21);
    cs->cd();
    dh2->Draw();


}
