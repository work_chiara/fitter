#include "FITMODEL.h"
#include "RooGaussian.h"
#include "RooPolynomial.h"

#include "ReadWriteInputFile.h"

using namespace std;
using namespace RooFit;

ClassImp(FITMODEL)

FITMODEL::FITMODEL(bool initParamater, ofstream *myLogger) : TObject() {

    observable_obj = new Observables();
    _myLogger = myLogger;
    globalSignal = new GlobalPdfSignal(initParamater, observable_obj, _myLogger);
    globalBackground_uds = new GlobalPdfBackground1(initParamater, observable_obj, _myLogger);
    globalBackground_charm = new GlobalPdfCharmBkg(initParamater, observable_obj, _myLogger);
    globalControlChannel = new GlobalPdfControlChannel(initParamater, observable_obj, _myLogger);
}

FITMODEL::~FITMODEL(){

    delete observable_obj;
    delete globalSignal;
    delete globalBackground_uds;
    delete globalBackground_charm;
    delete globalControlChannel;

}

RooDataSet *FITMODEL::genetateData(int numberEvents, RooAbsPdf *finalPdf) {

    RooRealVar *B__deltaE_corr = observable_obj->getObeservable(B__DELTAE_CORR);
    RooRealVar *B__Mbc_corr = observable_obj->getObeservable(B__MBC_CORR);
    RooRealVar *B__MK_var = observable_obj->getObeservable(B__MK);
    RooRealVar *B__MR_var = observable_obj->getObeservable(B__MR);
    RooRealVar *B_kst_hel = observable_obj->getObeservable(B__HELK);
    RooRealVar *B_rho0_hel = observable_obj->getObeservable(B__HELR);

    return finalPdf->generate(RooArgSet(*B__deltaE_corr, *B__Mbc_corr, *B__MK_var, *B__MR_var, *B_kst_hel, *B_rho0_hel),
                              numberEvents);
}

RooAbsPdf *FITMODEL::getPdfSingleObservable(string category, string nameObservable, string signalPolarization) {

    if (category.compare(SIGNAL) == 0) {
        return globalSignal->get_singlePDF_signal(nameObservable, signalPolarization);
    } else if (category.compare(UDS_BKG) == 0) {
        return globalBackground_uds->get_singlePDF_background1(nameObservable);
    } else if (category.compare(CHARM_BKG) == 0) {
        return globalBackground_charm->get_singlePDF_charm_bkg(nameObservable);
    } else if (category.compare(CONTROL_CHANNEL) == 0) {
        return globalControlChannel->get_singlePDF_controlChannel(nameObservable, signalPolarization);
    } else {
        return NULL;
    }

}


RooRealVar *FITMODEL::getObservable(string nameObservable) {
    RooRealVar *observable = observable_obj->getObeservable(nameObservable.c_str());
    return observable;
}

RooAbsPdf *FITMODEL::getPdf_ForTest_fixedParam(string nameObservable) {

    RooAbsPdf *bkg_pdf_fixParam = globalBackground_uds->get_singlePDF_background1(nameObservable.c_str());
    RooAbsPdf *sig_pdf_fixParam = globalSignal->get_singlePDF_signal(nameObservable.c_str(), "both");

    RooRealVar *f_sig = new RooRealVar("f_sig", "fraction of signal", 0., 1.);

    RooAbsPdf *model_fixParam = new RooAddPdf("model_fixParam", "model with fixed param",
                                              RooArgList(*sig_pdf_fixParam, *bkg_pdf_fixParam), *f_sig);

    return model_fixParam;
}


RooAbsPdf *FITMODEL::get_6observable_Signal_ForTest() {
    RooAbsPdf *sig_ln_pdf = globalSignal->createGlobalPdf_long();
    RooAbsPdf *sig_tr_pdf = globalSignal->createGlobalPdf_transv();

    RooRealVar *f_ln = new RooRealVar("f_ln", "fraction of longitudinal signal", 0., 1.);

    RooAbsPdf *model_fixParam = new RooAddPdf("model_fixParam", "model with fixed param",
                                              RooArgList(*sig_ln_pdf, *sig_tr_pdf), *f_ln);
    return model_fixParam;

}

RooAbsPdf *FITMODEL::get_6observable_Signal_And_Background_ForTest(double fln_value, double fsig_value) {
    // PDF signal
    RooAbsPdf *sig_ln_pdf = globalSignal->createGlobalPdf_long();
    RooAbsPdf *sig_tr_pdf = globalSignal->createGlobalPdf_transv();
    RooRealVar *f_ln = new RooRealVar("f_ln", "fraction of longitudinal signal", fln_value, 0., 1.);

    RooAbsPdf *model_fixParam_sig = new RooAddPdf("model_fixParam_sig", "model with fixed param",
                                                  RooArgList(*sig_ln_pdf, *sig_tr_pdf), *f_ln);

    // PDF bkg
    RooAbsPdf *bkg_pdf = globalBackground_uds->createGlobalPdf();

    RooRealVar *f_sig = new RooRealVar("f_sig_6D", "fraction of longitudinal signal 6D", fsig_value, 0., 1.);

    RooAbsPdf *model_fixParam = new RooAddPdf("model_fixParam_sig_and_bkg", "model with fixed param signal and bkg",
                                              RooArgList(*model_fixParam_sig, *bkg_pdf), *f_sig);
    return model_fixParam;

}


RooAbsPdf *FITMODEL::get_6observable_Signal_And_Background_ForTest_fixed_fraction() {
    // PDF signal
    RooAbsPdf *sig_ln_pdf = globalSignal->createGlobalPdf_long();
    RooAbsPdf *sig_tr_pdf = globalSignal->createGlobalPdf_transv();
    RooRealVar *f_ln = new RooRealVar("f_ln", "fraction of longitudinal signal", 0., 1.);

    ReadWriteInputFile *testRead = new ReadWriteInputFile("results/signal/global/fixedParameterFromPreviuosFit.txt");
    map<string, double> *mapTest = testRead->read();

    f_ln->setVal(mapTest->find("f_ln")->second);
    f_ln->setConstant();

    RooAbsPdf *model_fixParam_sig = new RooAddPdf("model_fixParam_sig", "model with fixed param",
                                                  RooArgList(*sig_ln_pdf, *sig_tr_pdf), *f_ln);

    // PDF bkg
    RooAbsPdf *bkg_pdf = globalBackground_uds->createGlobalPdf();

    RooRealVar *f_sig = new RooRealVar("f_sig_6D", "fraction of longitudinal signal 6D", 0., 1.);
    f_sig->setVal(mapTest->find("f_sig")->second);
    f_sig->setConstant();

    RooAbsPdf *model_fixParam = new RooAddPdf("model_fixParam_sig_and_bkg", "model with fixed param signal and bkg",
                                              RooArgList(*model_fixParam_sig, *bkg_pdf), *f_sig);
    return model_fixParam;

}

RooAbsPdf *FITMODEL::get_6observable_Signal_ForTest_fixed_fraction() {
    // PDF signal
    RooAbsPdf *sig_ln_pdf = globalSignal->createGlobalPdf_long();
    RooAbsPdf *sig_tr_pdf = globalSignal->createGlobalPdf_transv();
    RooRealVar *f_ln = new RooRealVar("f_ln_sig", "fraction of longitudinal signal", 0., 1.);

    ReadWriteInputFile *testRead = new ReadWriteInputFile("results/signal/global/fixedParameterFromPreviuosFit.txt");
    map<string, double> *mapTest = testRead->read();

    f_ln->setVal(mapTest->find("f_ln")->second);
    f_ln->setConstant();

    RooAbsPdf *model_fixParam_sig = new RooAddPdf("model_fixParam_sig", "model with fixed param",
                                                  RooArgList(*sig_ln_pdf, *sig_tr_pdf), *f_ln);

    return model_fixParam_sig;

}

RooAbsPdf *FITMODEL::finalModel_ForTest() {
    // PDF signal
    RooAbsPdf *sig_ln_pdf = globalSignal->createGlobalPdf_long();
    RooAbsPdf *sig_tr_pdf = globalSignal->createGlobalPdf_transv();
    RooRealVar *f_ln = new RooRealVar("f_ln_sig_final", "fraction of longitudinal signal", 0., 1.);

    RooAbsPdf *model_fixParam_sig = new RooAddPdf("model_fixParam_sig", "model with fixed param",
                                                  RooArgList(*sig_ln_pdf, *sig_tr_pdf), *f_ln);

    // PDF bkg
    RooAbsPdf *bkg_pdf = globalBackground_uds->createGlobalPdf();

    RooRealVar *f_sig = new RooRealVar("f_sig_6D_final", "fraction of longitudinal signal 6D", 0., 1.);

    RooAbsPdf *model_fixParam = new RooAddPdf("model_fixParam_sig_and_bkg", "model with fixed param signal and bkg",
                                              RooArgList(*model_fixParam_sig, *bkg_pdf), *f_sig);
    return model_fixParam;

}

