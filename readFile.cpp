// code to read file with workspace
#include "RooDataSet.h"
#include "RooWorkspace.h"
#include "TFile.h"
#include "TCanvas.h"

using namespace RooFit;

void readFile(){

    // Open input file with workspace
    TFile *f = new TFile("fileAfterPre_selection/tot_Bu_Rho0Kst+_K0spi+_Transverse_B2KstRho.root");

    RooWorkspace* w = (RooWorkspace*) f->Get("wspace");
    RooAbsData* data = w->data("dataset");
    RooRealVar *var = w->var(B__MBC_CORR);
    int Entries = data->numEntries();

    TCanvas* c = new TCanvas("c", "c", 800, 400);
    c->cd();
    RooPlot* var_frame = var->frame() ;
    data->plotOn(var_frame);

    cout << "numero di entries = " << Entries << endl;

    w->Print();
    data->Print();

}
