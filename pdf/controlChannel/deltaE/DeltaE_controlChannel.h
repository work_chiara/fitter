#ifndef DeltaE_controlChannel_NEW
#define DeltaE_controlChannel_NEW

#include "RooAddPdf.h"
#include "RooRealVar.h"

#include <string>
#include "../../../Observables.h"

#ifndef ROOT_TObject
#include "TObject.h"
#endif

using namespace std;

class DeltaE_controlChannel : public TObject{

RooRealVar* mean1;
RooRealVar* sigma1;
RooRealVar* mean2;
RooRealVar* sigma2;
RooRealVar* f_g1;
RooAbsPdf * gaussian1;
RooAbsPdf * gaussian2;
Observables *observable;
    RooRealVar *B__deltaE_corr;
ofstream* _myLogger;

public:
  DeltaE_controlChannel(Observables* observable_obj,ofstream* myLogger);
  RooAbsPdf* createPdfObservable();
  void print();
  void initParameterFix();

  ClassDef(DeltaE_controlChannel,1);

};

#endif
