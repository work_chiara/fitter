#include "DeltaE_controlChannel.h"
#include "RooGaussian.h"
#include "RooPolynomial.h"
#include <fstream>

#include "../../../ReadWriteInputFile.h"

using namespace std;

ClassImp(DeltaE_controlChannel)

DeltaE_controlChannel::DeltaE_controlChannel(Observables *observable_obj, ofstream *myLogger) : TObject() {

    observable = observable_obj;
    _myLogger = myLogger;
    ReadWriteInputFile *readWriteInputFile = new ReadWriteInputFile("pdf/controlChannel/deltaE/inputFile_deltaE_controlChannel.txt");
    map<string, double> *mapTest = readWriteInputFile->read();

    B__deltaE_corr = observable->getObeservable(B__DELTAE_CORR);

    double mean1Min = mapTest->find("mean1Min")->second;
    double mean1Max = mapTest->find("mean1Max")->second;
    double sigma1Min = mapTest->find("sigma1Min")->second;
    double sigma1Max = mapTest->find("sigma1Max")->second;

    double mean2Min = mapTest->find("mean2Min")->second;
    double mean2Max = mapTest->find("mean2Max")->second;
    double sigma2Min = mapTest->find("sigma2Min")->second;
    double sigma2Max = mapTest->find("sigma2Max")->second;

    double fMin = mapTest->find("fMin")->second;
    double fMax = mapTest->find("fMax")->second;

    double mean1_initParameter = mapTest->find("mean1_initParameter")->second;
    double sigma1_initParameter = mapTest->find("sigma1_initParameter")->second;
    double mean2_initParameter = mapTest->find("mean2_initParameter")->second;
    double sigma2_initParameter = mapTest->find("sigma2_initParameter")->second;
    double f_g1_initParameter = mapTest->find("f_g1_initParameter")->second;


    mean1 = new RooRealVar("mean1_controlChannel", "mean of the first gaussian for deltaE variable", mean1_initParameter, mean1Min, mean1Max);
    sigma1 = new RooRealVar("sigma1_controlChannel", "width of the first gaussian for deltaE variable", sigma1_initParameter, sigma1Min, sigma1Max);
    mean2 = new RooRealVar("mean2_controlChannel", "mean of the second gaussian for deltaE variable", mean2_initParameter, mean2Min, mean2Max);
    sigma2 = new RooRealVar("sigma2_controlChannel", "width of the second gaussian for deltaE variable", sigma2_initParameter, sigma2Min, sigma2Max);
    f_g1 = new RooRealVar("f_g1_controlChannel", "f_g1",f_g1_initParameter, fMin, fMax);

    *_myLogger << "DeltaE_controlChannel: mean1 [" << mean1Min << " , " << mean1Max << "]" << endl;
    *_myLogger << "DeltaE_controlChannel: mean2 [" << mean2Min << " , " << mean2Max << "]" << endl;
    *_myLogger << "DeltaE_controlChannel: sigma1 [" << sigma1Min << " , " << sigma1Max << "]" << endl;
    *_myLogger << "DeltaE_controlChannel: sigma2 [" << sigma2Min << " , " << sigma2Max << "]" << endl;
    *_myLogger << "DeltaE_controlChannel: f [" << fMin << " , " << fMax << "]" << endl;

    double mean1_fixedValue = mapTest->find("mean1_fixedValue")->second;
    double sigma1_fixedValue = mapTest->find("sigma1_fixedValue")->second;
    double mean2_fixedValue = mapTest->find("mean2_fixedValue")->second;
    double sigma2_fixedValue = mapTest->find("sigma2_fixedValue")->second;
    double f_g1_fixedValue = mapTest->find("f_g1_fixedValue")->second;

    if (mapTest->find("mean1_setFixedValue")->second) {
        mean1->setVal(mean1_fixedValue);
        mean1->setConstant();
    }
    if (mapTest->find("sigma1_setFixedValue")->second) {
        sigma1->setVal(sigma1_fixedValue);
        sigma1->setConstant();
    }
    if (mapTest->find("mean2_setFixedValue")->second) {
        mean2->setVal(mean2_fixedValue);
        mean2->setConstant();
    }
    if (mapTest->find("sigma2_setFixedValue")->second) {
        sigma2->setVal(sigma2_fixedValue);
        sigma2->setConstant();
    }
    if (mapTest->find("f_g1_setFixedValue")->second) {
        f_g1->setVal(f_g1_fixedValue);
        f_g1->setConstant();
    }



}

RooAbsPdf *DeltaE_controlChannel::createPdfObservable() {

    gaussian1 = new RooGaussian("gaussian1_controlChannel", "gaussian 1 PDF for deltaE variable", *B__deltaE_corr, *mean1, *sigma1);
    gaussian2 = new RooGaussian("gaussian2_controlChannel", "gaussian 2 PDF for deltaE variable", *B__deltaE_corr, *mean2, *sigma2);
    RooAbsPdf *doubleGaussian = new RooAddPdf("doubleGaussian_controlChannel", "double gaussian", RooArgList(*gaussian1, *gaussian2),
                                              *f_g1);
    return doubleGaussian;
}

void DeltaE_controlChannel::initParameterFix() {

    string B__DELTAE_CORR_Name(B__DELTAE_CORR);
    ReadWriteInputFile *testRead = new ReadWriteInputFile(
            "results/controlChannel/" + B__DELTAE_CORR_Name + "/fixedParameterFromPreviuosFit.txt");
    map<string, double> *mapTest = testRead->read();

    mean1->setVal(mapTest->find("mean1")->second);
    mean2->setVal(mapTest->find("mean2")->second);
    sigma1->setVal(mapTest->find("sigma1")->second);
    sigma2->setVal(mapTest->find("sigma2")->second);
    f_g1->setVal(mapTest->find("f")->second);

    mean1->setConstant();
    sigma1->setConstant();
    mean2->setConstant();
    sigma2->setConstant();
    f_g1->setConstant();

    *_myLogger << "DeltaE_controlChannel: mean1 = " << mean1->getValV() << endl;
    *_myLogger << "DeltaE_controlChannel: mean2 = " << mean2->getValV() << endl;
    *_myLogger << "DeltaE_controlChannel: sigma1 = " << sigma1->getValV() << endl;
    *_myLogger << "DeltaE_controlChannel: sigma2 = " << sigma2->getValV() << endl;
    *_myLogger << "DeltaE_controlChannel: f = " << f_g1->getValV() << endl;

}


void DeltaE_controlChannel::print() {
    mean1->Print();
    sigma1->Print();
    mean2->Print();
    sigma2->Print();
    f_g1->Print();
}


