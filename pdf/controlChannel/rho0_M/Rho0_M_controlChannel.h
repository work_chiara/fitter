#ifndef Rho0_M_controlChannel_NEW
#define Rho0_M_controlChannel_NEW

#include "RooAddPdf.h"
#include "RooRealVar.h"

#include "../../../Observables.h"

#ifndef ROOT_TObject
#include "TObject.h"
#endif

class Rho0_M_controlChannel : public TObject{

RooRealVar* mean_rho_M;
RooRealVar* width_rho_M;
RooRealVar* _spin;
RooRealVar* _radius;
RooRealVar* _mass_a;
RooRealVar* _mass_b;

Observables* observable;
ofstream* _myLogger;

public:
  Rho0_M_controlChannel(Observables* observable_obj,ofstream* myLogger);
  RooAbsPdf* createPdfObservable();
  void print();
  void initParameterFix();

  ClassDef(Rho0_M_controlChannel,1);

};

#endif
