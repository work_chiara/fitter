#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooAddPdf.h"
#include "RooProdPdf.h"
#include "TCanvas.h"
#include "RooPlot.h"
#include <fstream>

#include "../deltaE/DeltaE_controlChannel.h"
#include "../m_es/Mes_controlChannel.h"
#include "../kstMass/kstMass_controlChannel.h"
#include "../rho0_M/Rho0_M_controlChannel.h"
#include "../KstHel_long/KstHel_long_controlChannel.h"
#include "../KstHel_tr/KstHel_tr_controlChannel.h"
#include "../KstHel/KstHel_controlChannel.h"
#include "../rho0Hel_tr/rho0Hel_tr_controlChannel.h"
#include "../rho0Hel_long/rho0Hel_long_controlChannel.h"
#include "../rho0Hel/rho0Hel_controlChannel.h"

#include "GlobalPdfControlChannel.h"

using namespace std;
using namespace RooFit;

ClassImp(GlobalPdfControlChannel)

GlobalPdfControlChannel::GlobalPdfControlChannel(bool initParamater, Observables *observable_obj, ofstream *myLogger) : TObject() {
    observable = observable_obj;
    initialParamater = initParamater;
    _myLogger = myLogger;
}

RooAbsPdf *GlobalPdfControlChannel::createGlobalPdf_long() {
    *_myLogger << "GlobalPdfControlChannel: global pdf control channel build" << endl;

    RooAbsPdf *pdf_deltaE_controlChannel = get_singlePDF_controlChannel(B__DELTAE_CORR);
    RooAbsPdf *pdf_mes_controlChannel = get_singlePDF_controlChannel(B__MBC_CORR);
    RooAbsPdf *pdf_kstMass_controlChannel = get_singlePDF_controlChannel(B__MK);
    RooAbsPdf *pdf_Rho0_M_controlChannel = get_singlePDF_controlChannel(B__MR);
    RooAbsPdf *pdf_Ksthel_ln_controlChannel = get_singlePDF_controlChannel(B__HELK, LONGITUDINAL);
    RooAbsPdf *pdf_Rhohel_ln_controlChannel = get_singlePDF_controlChannel(B__HELR, LONGITUDINAL);

    RooProdPdf *global_pdf_controlChannel = new RooProdPdf("global_pdf_controlChannel_ln", "controlChannel PDF ln",
                                                   RooArgList(*pdf_mes_controlChannel, *pdf_deltaE_controlChannel,
                                                              *pdf_kstMass_controlChannel, *pdf_Rho0_M_controlChannel,
                                                              *pdf_Ksthel_ln_controlChannel, *pdf_Rhohel_ln_controlChannel));

    *_myLogger << "GlobalPdfControlChannel: create global longitudinal controlChannel pdf" << endl;
    return global_pdf_controlChannel;
}

RooAbsPdf *GlobalPdfControlChannel::createGlobalPdf_transv() {

    RooAbsPdf *pdf_deltaE_controlChannel = get_singlePDF_controlChannel(B__DELTAE_CORR);
    RooAbsPdf *pdf_mes_controlChannel = get_singlePDF_controlChannel(B__MBC_CORR);
    RooAbsPdf *pdf_kstMass_controlChannel = get_singlePDF_controlChannel(B__MK);
    RooAbsPdf *pdf_Rho0_M_controlChannel = get_singlePDF_controlChannel(B__MR);
    RooAbsPdf *pdf_Ksthel_tr_controlChannel = get_singlePDF_controlChannel(B__HELK, TRANSVERSE);
    RooAbsPdf *pdf_Rhohel_tr_controlChannel = get_singlePDF_controlChannel(B__HELR, TRANSVERSE);


    RooProdPdf *global_pdf_controlChannel = new RooProdPdf("global_pdf_controlChannel_tr", "controlChannel PDF tr",
                                                   RooArgList(*pdf_mes_controlChannel, *pdf_deltaE_controlChannel,
                                                              *pdf_kstMass_controlChannel, *pdf_Rho0_M_controlChannel,
                                                              *pdf_Ksthel_tr_controlChannel, *pdf_Rhohel_tr_controlChannel));

    *_myLogger << "GlobalPdfControlChannel: create global controlChannel trasversal pdf" << endl;
    return global_pdf_controlChannel;
}


RooAbsPdf *GlobalPdfControlChannel::get_singlePDF_controlChannel(string observableName, string controlChannelPolarization) {
    RooAbsPdf *pdf;

    if (observableName.compare(B__DELTAE_CORR) == 0) {
        DeltaE_controlChannel *DeltaE_controlChannel_obj = new DeltaE_controlChannel(observable, _myLogger);
        if (initialParamater) {
            DeltaE_controlChannel_obj->initParameterFix();
        }
        pdf = DeltaE_controlChannel_obj->createPdfObservable();
    } else if (observableName.compare(B__MBC_CORR) == 0) {
        Mes_controlChannel *mes_controlChannel_obj = new Mes_controlChannel(observable, _myLogger);
        if (initialParamater) {
            mes_controlChannel_obj->initParameterFix();
        }
        pdf = mes_controlChannel_obj->createPdfObservable();
    } else if (observableName.compare(B__MK) == 0) {
        kstMass_controlChannel *kstMass_controlChannel_obj = new kstMass_controlChannel(observable, _myLogger);
        if (initialParamater) {
            kstMass_controlChannel_obj->initParameterFix();
        }
        pdf = kstMass_controlChannel_obj->createPdfObservable();
    } else if (observableName.compare(B__MR) == 0) {
        Rho0_M_controlChannel *Rho0_M_controlChannel_obj = new Rho0_M_controlChannel(observable, _myLogger);
        if (initialParamater) {
            Rho0_M_controlChannel_obj->initParameterFix();
        }
        pdf = Rho0_M_controlChannel_obj->createPdfObservable();
    } else if (observableName.compare(B__HELK) == 0 && controlChannelPolarization.compare(BOTH) == 0) {
        KstHel_controlChannel *kstHel_controlChannel_obj = new KstHel_controlChannel(observable, _myLogger);
        if (initialParamater) {
            kstHel_controlChannel_obj->initParameterFix();
        }
        pdf = kstHel_controlChannel_obj->createPdfObservable();
    } else if (observableName.compare(B__HELK) == 0 && controlChannelPolarization.compare(LONGITUDINAL) ==
                                                           0) {
        cout << "in global controlChannel k ln " << endl;
        KstHel_long_controlChannel *kstHel_long_controlChannel_obj = new KstHel_long_controlChannel(observable, _myLogger);
        if (initialParamater) {
            kstHel_long_controlChannel_obj->initParameterFix();
        }
        pdf = kstHel_long_controlChannel_obj->createPdfObservable();
    } else if (observableName.compare(B__HELK) == 0 && controlChannelPolarization.compare(TRANSVERSE) == 0) {
        KstHel_tr_controlChannel *kstHel_tr_controlChannel_obj = new KstHel_tr_controlChannel(observable, _myLogger);
        if (initialParamater) {
            kstHel_tr_controlChannel_obj->initParameterFix();
        }
        pdf = kstHel_tr_controlChannel_obj->createPdfObservable();
    } else if (observableName.compare(B__HELR) == 0 && controlChannelPolarization.compare(BOTH) == 0) {
        rho0Hel_controlChannel *rho0Hel_controlChannel_obj = new rho0Hel_controlChannel(observable, _myLogger);
        if (initialParamater) {
            rho0Hel_controlChannel_obj->initParameterFix();
        }
        pdf = rho0Hel_controlChannel_obj->createPdfObservable();
    } else if (observableName.compare(B__HELR) == 0 && controlChannelPolarization.compare(LONGITUDINAL) == 0) {
        rho0Hel_long_controlChannel *rho0Hel_long_controlChannel_obj = new rho0Hel_long_controlChannel(observable, _myLogger);
        if (initialParamater) {
            rho0Hel_long_controlChannel_obj->initParameterFix();
        }
        pdf = rho0Hel_long_controlChannel_obj->createPdfObservable();
    } else if (observableName.compare(B__HELR) == 0 && controlChannelPolarization.compare(TRANSVERSE) == 0) {
        rho0Hel_tr_controlChannel *rho0Hel_tr_controlChannel_obj = new rho0Hel_tr_controlChannel(observable, _myLogger);
        if (initialParamater) {
            rho0Hel_tr_controlChannel_obj->initParameterFix();
        }
        pdf = rho0Hel_tr_controlChannel_obj->createPdfObservable();
    } else {
        cout << "ERROR: pdf not FOUND!" << observableName << endl;

    }
    return pdf;
}
