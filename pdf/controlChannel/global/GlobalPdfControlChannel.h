#ifndef POLARIZATION
#define POLARIZATION

static const char *const LONGITUDINAL = "longitudinal";

static const char *const TRANSVERSE = "transverse";

static const char *const BOTH = "both";
#endif
#ifndef Global_controlChannel_NEW
#define Global_controlChannel_NEW

#include "RooAddPdf.h"
#include "RooRealVar.h"
#include <fstream>

#include "../../../Observables.h"

#ifndef ROOT_TObject
#include "TObject.h"
#endif

class GlobalPdfControlChannel : public TObject {
    bool initialParamater = false;
    Observables *observable;
    ofstream *_myLogger;

public:
    GlobalPdfControlChannel(bool initParamater, Observables *observable_obj, ofstream* myLogger);
    RooAbsPdf *get_singlePDF_controlChannel(string observableName,string controlChannelPolarization = "");


    RooAbsPdf *createGlobalPdf_transv();
    RooAbsPdf *createGlobalPdf_long();


ClassDef(GlobalPdfControlChannel, 1);

};

#endif
