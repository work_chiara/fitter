#ifndef rho0Hel_long_controlChannel_NEW
#define rho0Hel_long_controlChannel_NEW

#include "RooAddPdf.h"
#include "RooRealVar.h"

#include "../../../Observables.h"

#ifndef ROOT_TObject
#include "TObject.h"
#endif

class rho0Hel_long_controlChannel : public TObject{

RooRealVar* a_rho_ln;
RooRealVar* b_rho_ln;
RooRealVar* c_rho_ln;
RooRealVar* d_rho_ln;

Observables* observable;
ofstream* _myLogger;

public:
  rho0Hel_long_controlChannel(Observables* observable_obj,ofstream* myLogger);
  RooAbsPdf* createPdfObservable();
  void print();
  void initParameterFix();

  ClassDef(rho0Hel_long_controlChannel,1);

};

#endif
