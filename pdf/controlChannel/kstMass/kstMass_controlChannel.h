#ifndef kstMass_controlChannel_NEW
#define kstMass_controlChannel_NEW

#include "RooAddPdf.h"
#include "RooRealVar.h"

#include <string>
#include "../../../Observables.h"

#ifndef ROOT_TObject
#include "TObject.h"
#endif

using namespace std;

class kstMass_controlChannel : public TObject{

RooRealVar* mean_kst_M;
RooRealVar* sigma_kst_M;
Observables *observable;
RooRealVar *B_kstMass;
ofstream* _myLogger;

public:
  kstMass_controlChannel(Observables* observable_obj,ofstream* myLogger);
  RooAbsPdf* createPdfObservable();
  void print();
  void initParameterFix();

  ClassDef(kstMass_controlChannel,1);

};

#endif
