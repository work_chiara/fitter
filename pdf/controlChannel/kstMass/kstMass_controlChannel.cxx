#include "kstMass_controlChannel.h"
#include "RooBreitWigner.h"
#include <fstream>

#include "../../../ReadWriteInputFile.h"

using namespace std;

ClassImp(kstMass_controlChannel)

kstMass_controlChannel::kstMass_controlChannel(Observables *observable_obj, ofstream *myLogger) : TObject() {

    observable = observable_obj;
    _myLogger = myLogger;
    ReadWriteInputFile *readWriteInputFile = new ReadWriteInputFile("pdf/controlChannel/kstMass/inputFile_kstMass_controlChannel.txt");
    map<string, double> *mapTest = readWriteInputFile->read();

    B_kstMass = observable->getObeservable(B__MK);

    double meanMin = mapTest->find("meanMin")->second;
    double meanMax = mapTest->find("meanMax")->second;
    double sigmaMin = mapTest->find("sigmaMin")->second;
    double sigmaMax = mapTest->find("sigmaMax")->second;

    double mean_initParameter = mapTest->find("mean_initParameter")->second;
    double sigma_initParameter = mapTest->find("sigma_initParameter")->second;

    mean_kst_M = new RooRealVar("mean_kst_M_controlChannel", "mean of the Breit-Wigner kstMass variable",mean_initParameter, meanMin, meanMax);
    sigma_kst_M = new RooRealVar("sigma_kst_M_controlChannel", "width of the Breit-Wigner kstMass variable",sigma_initParameter, sigmaMin, sigmaMax);

    *_myLogger << "kstMass_controlChannel: mean [" << meanMin << " , " << meanMax << "]" << endl;
    *_myLogger << "kstMass_controlChannel: sigma [" << sigmaMin << " , " << sigmaMax << "]" << endl;

    double mean_fixedValue = mapTest->find("mean_fixedValue")->second;
    double sigma_fixedValue = mapTest->find("sigma_fixedValue")->second;

    if (mapTest->find("mean_setFixedValue")->second) {
        mean_kst_M->setVal(mean_fixedValue);
        mean_kst_M->setConstant();
    }
    if (mapTest->find("sigma_setFixedValue")->second) {
        sigma_kst_M->setVal(sigma_fixedValue);
        sigma_kst_M->setConstant();
    }

}

RooAbsPdf *kstMass_controlChannel::createPdfObservable() {

    RooAbsPdf *breitWigner = new RooBreitWigner("breitWigner_kstM_controlChannel", "Breit-Wigner", *B_kstMass, *mean_kst_M,
                                                *sigma_kst_M);
    return breitWigner;
}

void kstMass_controlChannel::initParameterFix() {

    string B__MK_Name(B__MK);
    ReadWriteInputFile *testRead = new ReadWriteInputFile(
            "results/controlChannel/" + B__MK_Name + "/fixedParameterFromPreviuosFit.txt");
    map<string, double> *mapTest = testRead->read();

    mean_kst_M->setVal(mapTest->find("mean")->second);
    mean_kst_M->setConstant();
    sigma_kst_M->setVal(mapTest->find("sigma")->second);
    sigma_kst_M->setConstant();

    *_myLogger << "kstMass_controlChannel: mean = " << mean_kst_M->getValV() << endl;
    *_myLogger << "kstMass_controlChannel: sigma = " << sigma_kst_M->getValV() << endl;

}


void kstMass_controlChannel::print() {
    mean_kst_M->Print();
    sigma_kst_M->Print();
}
