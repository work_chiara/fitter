#include "KstHel_controlChannel.h"
#include "RooGaussian.h"
#include "RooCBShape.h"
#include "RooPolynomial.h"
#include <RooGenericPdf.h>
#include "RooProdPdf.h"
#include <fstream>

#include "../../../ReadWriteInputFile.h"

ClassImp(KstHel_controlChannel)

KstHel_controlChannel::KstHel_controlChannel(Observables *observable_obj, ofstream *myLogger) : TObject() {

    observable = observable_obj;
    _myLogger = myLogger;

    //ReadWriteInputFile *testRead = new ReadWriteInputFile("pdf/controlChannel/KstHel/inputFile_KstHel_controlChannel.txt");
    //map<string, double> *mapTest = testRead->read();

    string B__HELK_Name(B__HELK);
    ReadWriteInputFile *testReadln = new ReadWriteInputFile(
            "results/controlChannel/longitudinal/" + B__HELK_Name + "/fixedParameterFromPreviuosFit.txt");
    map<string, double> *mapTestln = testReadln->read();

    ReadWriteInputFile *testReadtr = new ReadWriteInputFile(
            "results/controlChannel/transverse/" + B__HELK_Name + "/fixedParameterFromPreviuosFit.txt");
    map<string, double> *mapTesttr = testReadtr->read();


    a_long = new RooRealVar("a_long_controlChannel", "a_long", mapTestln->find("a_kst_ln")->second);
    b_long = new RooRealVar("b_long_controlChannel", "b_long", mapTestln->find("b_kst_ln")->second);
    c_long = new RooRealVar("c_long_controlChannel", "c_long", mapTestln->find("c_kst_ln")->second);

    a_tr = new RooRealVar("a_tr_controlChannel", "a_tr", mapTesttr->find("a_kst_tr")->second);
    b_tr = new RooRealVar("b_tr_controlChannel", "b_tr", mapTesttr->find("b_kst_tr")->second);
    c_tr = new RooRealVar("c_tr_controlChannel", "c_tr", mapTesttr->find("c_kst_tr")->second);

    f_l = new RooRealVar("f_l_controlChannel", "f_l", 0, 1);

}

RooAbsPdf *KstHel_controlChannel::createPdfObservable() {

    RooRealVar *B__helK = observable->getObeservable(B__HELK);
    string B__HELK_Name(B__HELK);

    string ln_pow_function = B__HELK_Name + " * " + B__HELK_Name;
    string tr_pow_function = "1 - " + B__HELK_Name + " * " + B__HELK_Name;

    // longitudinal pdf
    RooAbsPdf *Kst_pow_ln = new RooGenericPdf("Kst_pow_ln_controlChannel", "Kst_pow_ln",
                                              ln_pow_function.c_str(), RooArgList(*B__helK));
    RooArgList *polyParamList_ln = new RooArgList(*a_long, *b_long, *c_long);
    RooAbsPdf *pol3_ln = new RooPolynomial("pol3_ln_controlChannel", "pol3_ln", *B__helK, *polyParamList_ln);
    RooProdPdf *KstHel_long_pdf = new RooProdPdf("KstHel_long_pdf_controlChannel", "KstHel_long_pdf",
                                                 RooArgList(*Kst_pow_ln, *pol3_ln));
    //transverse pdf
    RooAbsPdf *Kst_pow_tr = new RooGenericPdf("Kst_pow_tr_controlChannel", "Kst_pow_tr",
                                              tr_pow_function.c_str(),
                                              RooArgList(*B__helK));
    RooArgList *polyParamList_tr = new RooArgList(*a_tr, *b_tr, *c_tr);


    RooAbsPdf *pol3_tr = new RooPolynomial("pol3_tr_controlChannel", "pol3_tr", *B__helK, *polyParamList_tr);

    RooProdPdf *KstHel_tr_pdf = new RooProdPdf("KstHel_tr_pdf_controlChannel", "KstHel_tr_pdf", RooArgList(*Kst_pow_tr, *pol3_tr));

    RooArgList *pdfCategoryList = new RooArgList(*KstHel_long_pdf, *KstHel_tr_pdf);
    RooAbsPdf *KstHel_pdf = new RooAddPdf("KstHel_pdf_controlChannel", "KstHel_pdf", *pdfCategoryList, *f_l);

    return KstHel_pdf;
}

void KstHel_controlChannel::initParameterFix() {

    //ReadWriteInputFile* testRead = new ReadWriteInputFile("results/controlChannel/KstHel/fixedParameterFromPreviuosFit.txt");
    //map<string, double>* mapTest = testRead->read();

    //*_myLogger << "KstHel_controlChannel: B__Mbc_corr parameter pdf (gaussian) init: " << endl;
    cout << "KstHel_controlChannel::initParameterFix" << endl;
    f_l->setVal(4.5450e-01); // CHANGE TODO
    f_l->setConstant();
    cout << "KstHel_controlChannel::initParameterFix" << endl;
}

void KstHel_controlChannel::print() {

}
