#ifndef Mes_controlChannel_NEW
#define Mes_controlChannel_NEW

#include "RooAddPdf.h"
#include "RooRealVar.h"

#include "../../../Observables.h"

#ifndef ROOT_TObject
#include "TObject.h"
#endif

class Mes_controlChannel : public TObject{

RooRealVar* cbmean;
RooRealVar* cbsigma;
RooRealVar* alpha;
RooRealVar* n;

Observables* observable;
ofstream* _myLogger;

public:
  Mes_controlChannel(Observables* observable_obj,ofstream* myLogger);
  RooAbsPdf* createPdfObservable();
  void print();
  void initParameterFix();

  ClassDef(Mes_controlChannel,1);

};

#endif
