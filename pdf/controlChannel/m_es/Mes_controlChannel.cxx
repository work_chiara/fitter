#include "Mes_controlChannel.h"
#include "RooGaussian.h"
#include "RooCBShape.h"
#include "RooPolynomial.h"
#include <fstream>

#include "../../../ReadWriteInputFile.h"

ClassImp(Mes_controlChannel)

Mes_controlChannel::Mes_controlChannel(Observables *observable_obj, ofstream *myLogger) : TObject() {

    observable = observable_obj;
    _myLogger = myLogger;

    ReadWriteInputFile *testRead = new ReadWriteInputFile("pdf/controlChannel/m_es/inputFile_mes_controlChannel.txt");
    map<string, double> *mapTest = testRead->read();

    double cbmeanMin = mapTest->find("cbmeanMin")->second;
    double cbmeanMax = mapTest->find("cbmeanMax")->second;
    double cbsigmaMin = mapTest->find("cbsigmaMin")->second;
    double cbsigmaMax = mapTest->find("cbsigmaMax")->second;
    double alphaMin = mapTest->find("alphaMin")->second;
    double alphaMax = mapTest->find("alphaMax")->second;
    double nMin = mapTest->find("nMin")->second;
    double nMax = mapTest->find("nMax")->second;

    double cbmean_initParameter = mapTest->find("cbmean_initParameter")->second;
    double cbsigma_initParameter = mapTest->find("cbsigma_initParameter")->second;
    double alpha_initParameter = mapTest->find("alpha_initParameter")->second;
    double n_initParameter = mapTest->find("n_initParameter")->second;

    cbmean = new RooRealVar("cbmean_controlChannel", "mean of controlChannel m_bc crystal ball", cbmean_initParameter, cbmeanMin, cbmeanMax);
    cbsigma = new RooRealVar("cbsigma_controlChannel", "sigma of controlChannel m_bc crystal ball", cbsigma_initParameter, cbsigmaMin,
                             cbsigmaMax);
    alpha = new RooRealVar("alpha_controlChannel", "alpha of controlChannel m_bc crystal ball", alpha_initParameter, alphaMin, alphaMax);
    n = new RooRealVar("n_controlChannel", "n parameter of the controlChannel m_bc crystal ball", n_initParameter, nMin, nMax);


    *_myLogger << "Mbc_controlChannel: cbmean [" << cbmeanMin << " , " << cbmeanMax << "]" << endl;
    *_myLogger << "Mbc_controlChannel: cbsigma [" << cbsigmaMin << " , " << cbsigmaMax << "]" << endl;
    *_myLogger << "Mbc_controlChannel: alpha [" << alphaMin << " , " << alphaMax << "]" << endl;
    *_myLogger << "Mbc_controlChannel: n [" << nMin << " , " << nMax << "]" << endl;

    double cbmean_fixedValue = mapTest->find("cbmean_fixedValue")->second;
    double cbsigma_fixedValue = mapTest->find("cbsigma_fixedValue")->second;
    double alpha_fixedValue = mapTest->find("alpha_fixedValue")->second;
    double n_fixedValue = mapTest->find("n_fixedValue")->second;

    if (mapTest->find("cbmean_setFixedValue")->second) {
        cbmean->setVal(cbmean_fixedValue);
        cbmean->setConstant();
    }
    if (mapTest->find("cbsigma_setFixedValue")->second) {
        cbsigma->setVal(cbsigma_fixedValue);
        cbsigma->setConstant();
    }
    if (mapTest->find("alpha_setFixedValue")->second) {
        alpha->setVal(alpha_fixedValue);
        alpha->setConstant();
    }
    if (mapTest->find("n_setFixedValue")->second) {
        n->setVal(n_fixedValue);
        alpha->setConstant();
    }

}

RooAbsPdf *Mes_controlChannel::createPdfObservable() {

    RooRealVar *m_es = observable->getObeservable(B__MBC_CORR);

    RooCBShape *cball = new RooCBShape("cball_controlChannel", "crystal ball", *m_es, *cbmean, *cbsigma, *alpha, *n);

    return cball;
}

void Mes_controlChannel::initParameterFix() {

    string B__MBC_CORR_Name(B__MBC_CORR);
    ReadWriteInputFile *testRead = new ReadWriteInputFile(
            "results/controlChannel/" + B__MBC_CORR_Name + "/fixedParameterFromPreviuosFit.txt");
    map<string, double> *mapTest = testRead->read();

    *_myLogger << "Mbc_controlChannel: B__Mbc_corr parameter pdf (gaussian) init: " << endl;
    cbmean->setVal(mapTest->find("cbmean")->second);
    cbmean->setConstant();
    cbsigma->setVal(mapTest->find("cbsigma")->second);
    cbsigma->setConstant();
    alpha->setVal(mapTest->find("alpha")->second);
    alpha->setConstant();
    n->setVal(mapTest->find("n")->second);
    n->setConstant();

    *_myLogger << "Mes_controlChannel: cbmean = " << cbmean->getValV() << endl;
    *_myLogger << "Mes_controlChannel: cbsigma = " << cbsigma->getValV() << endl;
    *_myLogger << "Mes_controlChannel: alpha = " << alpha->getValV() << endl;
    *_myLogger << "Mes_controlChannel: n = " << n->getValV() << endl;
}

void Mes_controlChannel::print() {
    cbmean->Print();
    cbsigma->Print();
    alpha->Print();
    n->Print();
}
