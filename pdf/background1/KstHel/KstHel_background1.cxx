#include "KstHel_background1.h"
#include "RooPolynomial.h"
#include "RooChebychev.h"
#include <fstream>

#include "../../../ReadWriteInputFile.h"

ClassImp(KstHel_background1)

KstHel_background1::KstHel_background1(Observables *observable_obj, ofstream *myLogger) : TObject() {

    observable = observable_obj;
    _myLogger = myLogger;

    ReadWriteInputFile *testRead = new ReadWriteInputFile("pdf/background1/KstHel/inputFile_KstHel_background1.txt");
    map<string, double> *mapTest = testRead->read();

    double aMin = mapTest->find("aMin")->second;
    double aMax = mapTest->find("aMax")->second;
    double bMin = mapTest->find("bMin")->second;
    double bMax = mapTest->find("bMax")->second;
    double cMin = mapTest->find("cMin")->second;
    double cMax = mapTest->find("cMax")->second;

    double a_initParameter = mapTest->find("a_initParameter")->second;
    double b_initParameter = mapTest->find("b_initParameter")->second;
    double c_initParameter = mapTest->find("c_initParameter")->second;

    a = new RooRealVar("a_kst_hel_uds", "a_kst_hel_uds", a_initParameter, aMin, aMax);
    b = new RooRealVar("b_kst_hel_uds", "b_kst_hel_uds", b_initParameter, bMin, bMax);
    c = new RooRealVar("c_kst_hel_uds", "c_kst_hel_uds", c_initParameter, cMin, cMax);

    double a_fixedValue = mapTest->find("a_fixedValue")->second;
    double b_fixedValue = mapTest->find("b_fixedValue")->second;
    double c_fixedValue = mapTest->find("c_fixedValue")->second;


    if (mapTest->find("a_setFixedValue")->second) {
        a->setVal(a_fixedValue);
        a->setConstant();
    }
    if (mapTest->find("b_setFixedValue")->second) {
        b->setVal(b_fixedValue);
        b->setConstant();
    }
    if (mapTest->find("c_setFixedValue")->second) {
        c->setVal(c_fixedValue);
        c->setConstant();
    }

}

RooAbsPdf *KstHel_background1::createPdfObservable() {

    RooRealVar *B__helK = observable->getObeservable(B__HELK);

    RooArgList *polyParamList = new RooArgList(*a, *b, *c);
    RooAbsPdf *pol3 = new RooPolynomial("pol3_kst_hel_uds", "pol3_kst_hel_uds", *B__helK, *polyParamList);

    return pol3;
}

void KstHel_background1::initParameterFix() {

    string B__HELK_Name(B__HELK);
    ReadWriteInputFile *testRead = new ReadWriteInputFile(
            "results/background1/" + B__HELK_Name + "/fixedParameterFromPreviuosFit.txt");
    map<string, double> *mapTest = testRead->read();

    *_myLogger << "KstHel_uds_bkg: parameter pdf init: " << endl;
    a->setVal(mapTest->find("a")->second);
    a->setConstant();
    b->setVal(mapTest->find("b")->second);
    b->setConstant();
    c->setVal(mapTest->find("c")->second);
    c->setConstant();


    *_myLogger << "KstHel_uds_bkg: a = " << a->getValV() << endl;
    *_myLogger << "KstHel_uds_bkg: b = " << b->getValV() << endl;
    *_myLogger << "KstHel_uds_bkg: c = " << c->getValV() << endl;


}

void KstHel_background1::print() {
    a->Print();
    b->Print();
    c->Print();
}
