#ifndef KstHel_background1_NEW
#define KstHel_background1_NEW

#include "RooAddPdf.h"
#include "RooRealVar.h"

#include "../../../Observables.h"

#ifndef ROOT_TObject
#include "TObject.h"
#endif

class KstHel_background1 : public TObject{

  RooRealVar* a;
  RooRealVar* b;
  RooRealVar* c;

Observables* observable;
ofstream* _myLogger;

public:
  KstHel_background1(Observables* observable_obj,ofstream* myLogger);
  RooAbsPdf* createPdfObservable();
  void print();
  void initParameterFix();

  ClassDef(KstHel_background1,1);

};

#endif
