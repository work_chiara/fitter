#ifndef Rho0_M_background1_NEW
#define Rho0_M_background1_NEW

#include "RooAddPdf.h"
#include "RooRealVar.h"

#include "../../../Observables.h"

#ifndef ROOT_TObject
#include "TObject.h"
#endif

class Rho0_M_background1 : public TObject{

RooRealVar* a0;
RooRealVar* a1;
RooRealVar* f;
RooRealVar* meanBG;
RooRealVar* widthBG;
RooRealVar* fGauss;
RooRealVar* meanGauss;
RooRealVar* sigmaGauss;
Observables *observable;
ofstream* _myLogger;

public:
  Rho0_M_background1(Observables* observable_obj,ofstream* myLogger);
  RooAbsPdf* createPdfObservable();
  void print();
  void initParameterFix();

  ClassDef(Rho0_M_background1,1);

};

#endif
