#include "Rho0_M_background1.h"
#include "RooPolynomial.h"
#include "RooGaussian.h"
#include "RooBreitWigner.h"
#include <fstream>

#include "../../../ReadWriteInputFile.h"

ClassImp(Rho0_M_background1)

Rho0_M_background1::Rho0_M_background1(Observables *observable_obj, ofstream *myLogger) : TObject() {

    observable = observable_obj;
    _myLogger = myLogger;

    ReadWriteInputFile *testRead = new ReadWriteInputFile("pdf/background1/rho0_M/inputFile_Rho0_M_background1.txt");
    map<string, double> *mapTest = testRead->read();

    double a0Min = mapTest->find("a0Min")->second;
    double a0Max = mapTest->find("a0Max")->second;
    double a1Min = mapTest->find("a1Min")->second;
    double a1Max = mapTest->find("a1Max")->second;
    double fMin = mapTest->find("fMin")->second;
    double fMax = mapTest->find("fMax")->second;

    double a0_initParameter = mapTest->find("a0_initParameter")->second;
    double a1_initParameter = mapTest->find("a1_initParameter")->second;
    double f_initParameter = mapTest->find("f_initParameter")->second;

    double a0_fixedValue = mapTest->find("a0_fixedValue")->second;
    double a1_fixedValue = mapTest->find("a1_fixedValue")->second;
    double f_fixedValue = mapTest->find("f_fixedValue")->second;

    *_myLogger << "rho0_M_uds_bkg: map size  =  " << mapTest->size() << endl;
    *_myLogger << "rho0_M_uds_bkg: rho0 mass" << endl;
    *_myLogger << "rho0_M_uds_bkg: a0 [" << a0Min << " , " << a0Max << "]" << endl;
    *_myLogger << "rho0_M_uds_bkg: a1 [" << a1Min << " , " << a1Max << "]" << endl;
    *_myLogger << "rho0_M_uds_bkg: f [" << fMin << " , " << fMax << "]" << endl;

    a0 = new RooRealVar("a0_rhoM_uds", "first parameter", a0_initParameter, a0Min, a0Max);
    a1 = new RooRealVar("a1_rhoM_uds", "first parameter", a1_initParameter, a1Min, a1Max);
    f = new RooRealVar("f_rhoM_uds", "f", f_initParameter, fMin, fMax);


    if (mapTest->find("a0_setFixedValue")->second) {
        a0->setVal(a0_fixedValue);
        a0->setConstant();
    }
    if (mapTest->find("a1_setFixedValue")->second) {
        a1->setVal(a1_fixedValue);
        a1->setConstant();
    }
    if (mapTest->find("f_setFixedValue")->second) {
        f->setVal(f_fixedValue);
        f->setConstant();
    }


}

RooAbsPdf *Rho0_M_background1::createPdfObservable() {

    string B__MR_Name(B__MR);
    ReadWriteInputFile *readFromSignalResults = new ReadWriteInputFile(
            "results/signal/" + B__MR_Name + "/fixedParameterFromPreviuosFit.txt");
    map<string, double> *mapSignalResults = readFromSignalResults->read();

    double meanValue = mapSignalResults->find("mean_rho_M")->second;
    double sigmaValue = mapSignalResults->find("width_rho_M")->second;

    meanBG = new RooRealVar("meanBG", "meanBG", meanValue);
    meanBG->setConstant();
    widthBG = new RooRealVar("widthBG", "widthBG", sigmaValue);
    widthBG->setConstant();

    RooRealVar *B__MR_var = observable->getObeservable(B__MR);

    RooArgList *polyParamList = new RooArgList(*a0, *a1);
    RooAbsPdf *pol2 = new RooPolynomial("pol2_rhoM_uds", "pol2", *B__MR_var, *polyParamList);
    RooAbsPdf *breitWigner = new RooBreitWigner("breitWigner_rhoM_uds", "Breit-Wigner", *B__MR_var, *meanBG, *widthBG);
    RooAbsPdf *polAndBW = new RooAddPdf("polAndBW_rhoM_uds", "1st order poly and BW from signal",
                                        RooArgList(*pol2, *breitWigner), RooArgList(*f));

    return polAndBW;

}

void Rho0_M_background1::initParameterFix() {

    string B__MR_Name(B__MR);
    ReadWriteInputFile *testRead = new ReadWriteInputFile(
            "results/background1/" + B__MR_Name + "/fixedParameterFromPreviuosFit.txt");
    map<string, double> *mapTest = testRead->read();

    *_myLogger << "Rho0_M_uds_bkg: rho0mass parameter pdf (gaussian) init: " << endl;
    a0->setVal(mapTest->find("a0")->second);
    a0->setConstant();
    f->setVal(mapTest->find("f")->second);
    f->setConstant();

    ReadWriteInputFile *readFromSignalResults = new ReadWriteInputFile(
            "results/signal/" + B__MR_Name + "/fixedParameterFromPreviuosFit.txt");
    map<string, double> *mapSignalResults = readFromSignalResults->read();

    meanBG->setVal(mapSignalResults->find("mean_rho_M")->second);
    meanBG->setConstant();
    widthBG->setVal(mapSignalResults->find("width_rho_M")->second);
    widthBG->setConstant();

    *_myLogger << "Rho0_M_uds_bkg: a0 = " << a0->getValV() << endl;
    *_myLogger << "Rho0_M_uds_bkg: f = " << f->getValV() << endl;
    *_myLogger << "Rho0_M_uds_bkg: meanBG (fixed to signal parameter value) = " << meanBG->getValV() << endl;
    *_myLogger << "Rho0_M_uds_bkg: widthBG (fixed to signal parameter value) = " << widthBG->getValV() << endl;

}

void Rho0_M_background1::print() {
    a0->Print();
    f->Print();
    meanBG->Print();
    widthBG->Print();
}
