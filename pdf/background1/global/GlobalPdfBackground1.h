#ifndef Global_background1_NEW
#define Global_background1_NEW

#include "RooAddPdf.h"
#include "RooRealVar.h"
#include <fstream>

#include "../deltaE/DeltaE_background1.h"
#include "../m_es/Mes_background1.h"
#include "../kstMass/kstMass_background1.h"
#include "../rho0_M/Rho0_M_background1.h"
#include "../../../Observables.h"

#ifndef ROOT_TObject
#include "TObject.h"
#endif

class GlobalPdfBackground1 : public TObject {
    bool initialParamater = false;
    Observables *observable;
    ofstream* _myLogger;

public:

    GlobalPdfBackground1(bool initParamater,Observables* observable_obj,ofstream* myLogger);
    RooAbsPdf *get_singlePDF_background1(string observableName);

    RooAbsPdf *createGlobalPdf();


ClassDef(GlobalPdfBackground1, 1);

};

#endif
