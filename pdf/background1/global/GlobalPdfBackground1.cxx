#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooAddPdf.h"
#include "RooProdPdf.h"
#include "TCanvas.h"
#include "RooPlot.h"
#include <fstream>

#include "../deltaE/DeltaE_background1.h"
#include "../m_es/Mes_background1.h"
#include "../kstMass/kstMass_background1.h"
#include "../rho0_M/Rho0_M_background1.h"
#include "../KstHel/KstHel_background1.h"
#include "../rho0Hel/rho0Hel_background1.h"
#include "GlobalPdfBackground1.h"


using namespace std;
using namespace RooFit;

ClassImp(GlobalPdfBackground1)


GlobalPdfBackground1::GlobalPdfBackground1(bool initParamater, Observables *observable_obj,ofstream* myLogger) : TObject() {
    initialParamater = initParamater;
    observable = observable_obj;
    _myLogger = myLogger;
}

RooAbsPdf *GlobalPdfBackground1::createGlobalPdf() {

    RooAbsPdf *pdf_deltaE_Background1 = get_singlePDF_background1(B__DELTAE_CORR);
    RooAbsPdf *pdf_mes_Background1 = get_singlePDF_background1(B__MBC_CORR);
    RooAbsPdf *pdf_kstMass_Background1 = get_singlePDF_background1(B__MK);
    RooAbsPdf *pdf_Rho0_M_Background1 = get_singlePDF_background1(B__MR);
    RooAbsPdf *pdf_Kst_hel_Background1 = get_singlePDF_background1(B__HELK);
    RooAbsPdf *pdf_Rho0_hel_Background1 = get_singlePDF_background1(B__HELR);

    // PRODOTTO DI SOLE 2 PDF
    //RooProdPdf *global_pdf_background1 = new RooProdPdf("global_pdf_background1", "background 1 PDF",*pdf_deltaE_Background1, *pdf_kstMass_Background1);
    //RooProdPdf *global_pdf_background1 = new RooProdPdf("global_pdf_background1", "background 1 PDF",*pdf_mes_Background1, *pdf_kstMass_Background1);
    //RooProdPdf *global_pdf_background1 = new RooProdPdf("global_pdf_background1", "background 1 PDF",*pdf_mes_Background1, *pdf_deltaE_Background1);
    RooProdPdf *global_pdf_background1 = new RooProdPdf("global_pdf_background1_6var", "background 6 PDF",RooArgList(*pdf_deltaE_Background1,*pdf_mes_Background1,*pdf_kstMass_Background1,*pdf_Rho0_M_Background1,*pdf_Kst_hel_Background1,*pdf_Rho0_hel_Background1));


    return global_pdf_background1;
}

RooAbsPdf *GlobalPdfBackground1::get_singlePDF_background1(string observableName) {
    RooAbsPdf *pdf_observable_Background1;

    if (observableName.compare(B__DELTAE_CORR) == 0) {
        DeltaE_background1 * pBackground1 = new DeltaE_background1(observable,_myLogger);
        if (initialParamater) {
            pBackground1->initParameterFix();
        }
        pdf_observable_Background1 = pBackground1->createPdfObservable();
    } else if (observableName.compare(B__MBC_CORR) == 0) {
        Mes_background1 *Mes_background1_obj = new Mes_background1(observable,_myLogger);

        if (initialParamater) {
            Mes_background1_obj->initParameterFix();
        }
        pdf_observable_Background1 = Mes_background1_obj->createPdfObservable();
    } else if (observableName.compare(B__MK) == 0) {
        kstMass_background1 *kstMass_background1_obj = new kstMass_background1(observable,_myLogger);

        if (initialParamater) {
            kstMass_background1_obj->initParameterFix();
        }
        pdf_observable_Background1 = kstMass_background1_obj->createPdfObservable();
    } else if (observableName.compare(B__MR) == 0) {
        Rho0_M_background1 *B_rho0_M_background1_obj = new Rho0_M_background1(observable,_myLogger);

        if (initialParamater) {
            B_rho0_M_background1_obj->initParameterFix();
        }
        pdf_observable_Background1 = B_rho0_M_background1_obj->createPdfObservable();
    } else if (observableName.compare(B__HELK) == 0) {
        KstHel_background1 *KstHel_background1_obj = new KstHel_background1(observable,_myLogger);
        if (initialParamater) {
            KstHel_background1_obj->initParameterFix();
        }
        pdf_observable_Background1 = KstHel_background1_obj->createPdfObservable();
    } else if (observableName.compare(B__HELR) == 0) {
        rho0Hel_background1 *rho0Hel_background1_obj = new rho0Hel_background1(observable,_myLogger);
        if (initialParamater) {
            rho0Hel_background1_obj->initParameterFix();
        }
        pdf_observable_Background1 = rho0Hel_background1_obj->createPdfObservable();
    }

    return pdf_observable_Background1;
}
