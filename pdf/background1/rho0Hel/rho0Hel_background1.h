#ifndef rho0Hel_background1_NEW
#define rho0Hel_background1_NEW

#include "RooAddPdf.h"
#include "RooRealVar.h"

#include "../../../Observables.h"

#ifndef ROOT_TObject
#include "TObject.h"
#endif

class rho0Hel_background1 : public TObject{

  RooRealVar* a;
  RooRealVar* b;
  RooRealVar* c;

Observables* observable;
ofstream* _myLogger;

public:
  rho0Hel_background1(Observables* observable_obj,ofstream* myLogger);
  RooAbsPdf* createPdfObservable();
  void print();
  void initParameterFix();

  ClassDef(rho0Hel_background1,1);

};

#endif
