#include "rho0Hel_background1.h"
#include "RooPolynomial.h"
#include "RooChebychev.h"
#include <fstream>

#include "../../../ReadWriteInputFile.h"

ClassImp(rho0Hel_background1)

rho0Hel_background1::rho0Hel_background1(Observables *observable_obj, ofstream *myLogger) : TObject() {

    observable = observable_obj;
    _myLogger = myLogger;

    ReadWriteInputFile *testRead = new ReadWriteInputFile("pdf/background1/rho0Hel/inputFile_rho0Hel_background1.txt");
    map<string, double> *mapTest = testRead->read();

    double aMin = mapTest->find("aMin")->second;
    double aMax = mapTest->find("aMax")->second;
    double bMin = mapTest->find("bMin")->second;
    double bMax = mapTest->find("bMax")->second;
    double cMin = mapTest->find("cMin")->second;
    double cMax = mapTest->find("cMax")->second;

    double a_initParameter = mapTest->find("a_initParameter")->second;
    double b_initParameter = mapTest->find("b_initParameter")->second;
    double c_initParameter = mapTest->find("c_initParameter")->second;

    a = new RooRealVar("a_rho_hel_uds", "a_rho_hel_uds", a_initParameter, aMin, aMax);
    b = new RooRealVar("b_rho_hel_uds", "b_rho_hel_uds", b_initParameter, bMin, bMax);
    c = new RooRealVar("c_rho_hel_uds", "c_rho_hel_uds", c_initParameter, cMin, cMax);

    double a_fixedValue = mapTest->find("a_fixedValue")->second;
    double b_fixedValue = mapTest->find("b_fixedValue")->second;
    double c_fixedValue = mapTest->find("c_fixedValue")->second;

    if (mapTest->find("a_setFixedValue")->second) {
        a->setVal(a_fixedValue);
        a->setConstant();
    }
    if (mapTest->find("b_setFixedValue")->second) {
        b->setVal(b_fixedValue);
        b->setConstant();
    }
    if (mapTest->find("c_setFixedValue")->second) {
        c->setVal(c_fixedValue);
        c->setConstant();
    }


}

RooAbsPdf *rho0Hel_background1::createPdfObservable() {

    RooRealVar *B__helR = observable->getObeservable(B__HELR);

    RooArgList *polyParamList = new RooArgList(*a, *b, *c);
    RooAbsPdf *pol3 = new RooChebychev("pol3_rho_hel_uds", "pol3_rho_hel_uds", *B__helR, *polyParamList);

    return pol3;
}

void rho0Hel_background1::initParameterFix() {

    string B__HELR_Name(B__HELR);
    ReadWriteInputFile *testRead = new ReadWriteInputFile(
            "results/background1/" + B__HELR_Name + "/fixedParameterFromPreviuosFit.txt");
    map<string, double> *mapTest = testRead->read();

    *_myLogger << "rho0Hel_uds_bkg: parameter pdf init: " << endl;
    a->setVal(mapTest->find("a")->second);
    a->setConstant();
    b->setVal(mapTest->find("b")->second);
    b->setConstant();
    c->setVal(mapTest->find("c")->second);
    c->setConstant();


    *_myLogger << "rho0Hel_long_uds_bkg: a = " << a->getValV() << endl;
    *_myLogger << "rho0Hel_long_uds_bkg: b = " << b->getValV() << endl;
    *_myLogger << "rho0Hel_long_uds_bkg: c = " << c->getValV() << endl;


}

void rho0Hel_background1::print() {
    a->Print();
    b->Print();
    c->Print();
}
