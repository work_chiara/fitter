#ifndef kstMass_background1_NEW
#define kstMass_background1_NEW

#include "RooAddPdf.h"
#include "RooRealVar.h"

#include "../../../Observables.h"

#ifndef ROOT_TObject
#include "TObject.h"
#endif

class kstMass_background1 : public TObject{

RooRealVar* a0;
RooRealVar* a1;
RooRealVar* f;
RooRealVar* mean;
RooRealVar* sigma;
Observables *observable;
ofstream* _myLogger;

public:
  kstMass_background1(Observables* observable_obj,ofstream* myLogger);
  RooAbsPdf* createPdfObservable();
  void print();
  void initParameterFix();

  ClassDef(kstMass_background1,1);

};

#endif
