#ifndef Mes_background1_NEW
#define Mes_background1_NEW

#include "RooAddPdf.h"
#include "RooRealVar.h"

#include "../../../Observables.h"

#ifndef ROOT_TObject
#include "TObject.h"
#endif

class Mes_background1 : public TObject{

RooRealVar* m0;
RooRealVar* c;
Observables *observable;
ofstream* _myLogger;

public:
  Mes_background1(Observables* observable_obj,ofstream* myLogger);
  RooAbsPdf* createPdfObservable();
  void print();
  void initParameterFix();

  ClassDef(Mes_background1,1);

};

#endif
