#include "Mes_background1.h"
#include "RooArgusBG.h"
#include <fstream>

#include "../../../ReadWriteInputFile.h"

ClassImp(Mes_background1)

Mes_background1::Mes_background1(Observables *observable_obj, ofstream *myLogger) : TObject() {

    observable = observable_obj;
    _myLogger = myLogger;

    ReadWriteInputFile *testRead = new ReadWriteInputFile("pdf/background1/m_es/inputFile_mes_background1.txt");
    map<string, double> *mapTest = testRead->read();

    double m0Min = mapTest->find("m0Min")->second;
    double m0Max = mapTest->find("m0Max")->second;
    double cMin = mapTest->find("cMin")->second;
    double cMax = mapTest->find("cMax")->second;

    double m0_initParameter = mapTest->find("m0_initParameter")->second;
    double c_initParameter = mapTest->find("c_initParameter")->second;

    double m0_fixedValue = mapTest->find("m0_fixedValue")->second;
    double c_fixedValue = mapTest->find("c_fixedValue")->second;

    m0 = new RooRealVar("m0_Bmass_bkg", "m0 of Argus function B__Mbc_corr uds background",m0_initParameter, m0Min, m0Max);
    c = new RooRealVar("c_Bmass_bkg", "c of Argus function B__Mbc_corr uds background", c_initParameter, cMin, cMax);

    if(mapTest->find("m0_setFixedValue")->second) {
        m0->setVal(m0_fixedValue);
        m0->setConstant();
    }
    if(mapTest->find("c_setFixedValue")->second) {
        c->setVal(c_fixedValue);
        c->setConstant();
    }


}

RooAbsPdf *Mes_background1::createPdfObservable() {

    RooRealVar *m_es = observable->getObeservable(B__MBC_CORR);

    RooAbsPdf *argus = new RooArgusBG("argus_Bmass_bkg", "Argus function for B__Mbc_corr uds background", *m_es, *m0,
                                      *c);
    return argus;
}

void Mes_background1::initParameterFix() {

    string B__MBC_CORR_Name(B__MBC_CORR);
    ReadWriteInputFile *testRead = new ReadWriteInputFile(
            "results/background1/" + B__MBC_CORR_Name + "/fixedParameterFromPreviuosFit.txt");
    map<string, double> *mapTest = testRead->read();

    *_myLogger << "Mes_background1: B__Mbc_corr parameter pdf (gaussian) init: " << endl;
    m0->setVal(mapTest->find("m0")->second);
    m0->setConstant();
    c->setVal(mapTest->find("c")->second);
    c->setConstant();

    *_myLogger << "Mes_background1: m0 = " << m0->getValV() << endl;
    *_myLogger << "Mes_background1: c = " << c->getValV() << endl;

}

void Mes_background1::print() {
    m0->Print();
    c->Print();
}
