#include "DeltaE_background1.h"
#include "RooPolynomial.h"
#include "RooChebychev.h"
#include <fstream>
#include "../../../ReadWriteInputFile.h"

ClassImp(DeltaE_background1)

DeltaE_background1::DeltaE_background1(Observables *observable_obj, ofstream *myLogger) : TObject() {

    observable = observable_obj;
    _myLogger = myLogger;
    ReadWriteInputFile *testRead = new ReadWriteInputFile("pdf/background1/deltaE/inputFile_deltaE_background1.txt");
    map<string, double> *mapTest = testRead->read();

    double a0min = mapTest->find("a0min")->second;
    double a0max = mapTest->find("a0max")->second;
    double a1min = mapTest->find("a1min")->second;
    double a1max = mapTest->find("a1max")->second;
    double a2min = mapTest->find("a2min")->second;
    double a2max = mapTest->find("a2max")->second;

    double a0_initParameter = mapTest->find("a0_initParameter")->second;
    double a1_initParameter = mapTest->find("a1_initParameter")->second;
    double a2_initParameter = mapTest->find("a2_initParameter")->second;

    double a0_fixedValue = mapTest->find("a0_fixedValue")->second;
    double a1_fixedValue = mapTest->find("a1_fixedValue")->second;
    double a2_fixedValue = mapTest->find("a2_fixedValue")->second;

    *_myLogger << "DeltaE_background1: map size  =  " << mapTest->size() << endl;

    *_myLogger << "DeltaE_background1: delta E background1" << endl;
    *_myLogger << "DeltaE_background1: a0 [" << a0min << " , " << a0max << "]" << endl;
    *_myLogger << "DeltaE_background1: a1 [" << a1min << " , " << a1max << "]" << endl;
    *_myLogger << "DeltaE_background1: a2 [" << a2min << " , " << a2max << "]" << endl;

    a0 = new RooRealVar("a0_deltaE_bkg", "first parameter", a0_initParameter, a0min, a0max);
    a1 = new RooRealVar("a1_deltaE_bkg", "second parameter", a1_initParameter, a1min, a1max);
    a2 = new RooRealVar("a2_deltaE_bkg", "third parameter", a2_initParameter, a2min, a2max);

    if(mapTest->find("a0_setFixedValue")->second) {
        a0->setVal(a0_fixedValue);
        a0->setConstant();
    }
    if(mapTest->find("a1_setFixedValue")->second) {
        a1->setVal(a1_fixedValue);
        a1->setConstant();
    }
    if(mapTest->find("a2_setFixedValue")->second) {
        a2->setVal(a2_fixedValue);
        a2->setConstant();
    }
}

RooAbsPdf *DeltaE_background1::createPdfObservable() {

    RooRealVar *deltaE = observable->getObeservable(B__DELTAE_CORR);
    RooArgList *polyParamList = new RooArgList(*a0, *a1, *a2);
    // signal PDF for the x observable
    //RooAbsPdf *pol2 = new RooPolynomial("pol_deltaE_bkg", "gaussian PDF for deltaE variable", *deltaE, *polyParamList);
    RooAbsPdf *pol2 = new RooChebychev("pol_deltaE_bkg", "gaussian PDF for deltaE variable", *deltaE, *polyParamList);

    return pol2;
}

void DeltaE_background1::initParameterFix() {

    string B__DELTAE_CORR_Name(B__DELTAE_CORR);
    ReadWriteInputFile *testRead = new ReadWriteInputFile(
            "results/background1/" + B__DELTAE_CORR_Name + "/fixedParameterFromPreviuosFit.txt");
    map<string, double> *mapTest = testRead->read();

    *_myLogger << "DeltaE_background1: deltaE parameter pdf (poly2) init: " << endl;
    a0->setVal(mapTest->find("a0")->second);
    a0->setConstant();
    a1->setVal(mapTest->find("a1")->second);
    a1->setConstant();
    a2->setVal(mapTest->find("a2")->second);
    a2->setConstant();
    *_myLogger << "DeltaE_background1: a0 = " << a0->getValV() << endl;
    *_myLogger << "DeltaE_background1: a1 = " << a1->getValV() << endl;
    *_myLogger << "DeltaE_background1: a2 = " << a2->getValV() << endl;
}

void DeltaE_background1::print() {
    a0->Print();
    a1->Print();
    a2->Print();
}
