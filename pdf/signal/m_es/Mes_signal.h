#ifndef Mes_signal_NEW
#define Mes_signal_NEW

#include "RooAddPdf.h"
#include "RooRealVar.h"

#include "../../../Observables.h"

#ifndef ROOT_TObject
#include "TObject.h"
#endif

class Mes_signal : public TObject{

RooRealVar* cbmean;
RooRealVar* cbsigma;
RooRealVar* alpha;
RooRealVar* n;

Observables* observable;
ofstream* _myLogger;

public:
  Mes_signal(Observables* observable_obj,ofstream* myLogger);
  RooAbsPdf* createPdfObservable();
  void print();
  void initParameterFix();

  ClassDef(Mes_signal,1);

};

#endif
