#include "KstHel_tr_signal.h"
#include "RooGaussian.h"
#include "RooCBShape.h"
#include "RooPolynomial.h"
#include "RooProdPdf.h"
#include <RooGenericPdf.h>
#include <fstream>

#include "../../../ReadWriteInputFile.h"

ClassImp(KstHel_tr_signal)

KstHel_tr_signal::KstHel_tr_signal(Observables *observable_obj, ofstream *myLogger) : TObject() {

    observable = observable_obj;
    _myLogger = myLogger;

    ReadWriteInputFile *testRead = new ReadWriteInputFile("pdf/signal/KstHel_tr/inputFile_KstHel_tr_signal.txt");
    map<string, double> *mapTest = testRead->read();

    double aMin = mapTest->find("aMin")->second;
    double aMax = mapTest->find("aMax")->second;
    double bMin = mapTest->find("bMin")->second;
    double bMax = mapTest->find("bMax")->second;
    double cMin = mapTest->find("cMin")->second;
    double cMax = mapTest->find("cMax")->second;

    double a_initParameter = mapTest->find("a_initParameter")->second;
    double b_initParameter = mapTest->find("b_initParameter")->second;
    double c_initParameter = mapTest->find("c_initParameter")->second;


    a_kst_tr = new RooRealVar("a_kst_tr", "a_kst_tr", a_initParameter, aMin, aMax);
    b_kst_tr = new RooRealVar("b_kst_tr", "b_kst_tr", b_initParameter, bMin, bMax);
    c_kst_tr = new RooRealVar("c_kst_tr", "c_kst_tr", c_initParameter, cMin, cMax);


    double a_fixedValue = mapTest->find("a_fixedValue")->second;
    double b_fixedValue = mapTest->find("b_fixedValue")->second;
    double c_fixedValue = mapTest->find("c_fixedValue")->second;

    if (mapTest->find("a_setFixedValue")->second) {
        a_kst_tr->setVal(a_fixedValue);
        a_kst_tr->setConstant();
    }
    if (mapTest->find("b_setFixedValue")->second) {
        b_kst_tr->setVal(b_fixedValue);
        b_kst_tr->setConstant();
    }
    if (mapTest->find("c_setFixedValue")->second) {
        c_kst_tr->setVal(c_fixedValue);
        c_kst_tr->setConstant();
    }

}

RooAbsPdf *KstHel_tr_signal::createPdfObservable() {

    RooRealVar *B__helK = observable->getObeservable(B__HELK);
    string B__HELK_Name(B__HELK);
    string tr_pow_function = "1 - " + B__HELK_Name + " * " + B__HELK_Name;


    RooAbsPdf *Kst_pow = new RooGenericPdf("Kst_pow_tr", "Kst_pow_tr",
                                           tr_pow_function.c_str(),
                                           RooArgList(*B__helK));
    RooArgList *polyParamList = new RooArgList(*a_kst_tr, *b_kst_tr, *c_kst_tr);
    RooAbsPdf *pol3 = new RooPolynomial("pol3_tr", "pol3_tr", *B__helK, *polyParamList);
    RooProdPdf *KstHel_tr_pdf = new RooProdPdf("KstHel_tr_pdf", "KstHel_tr_pdf", RooArgList(*Kst_pow, *pol3));

    return KstHel_tr_pdf;
}

void KstHel_tr_signal::initParameterFix() {

    string B__HELK_Name(B__HELK);

    ReadWriteInputFile *testRead = new ReadWriteInputFile(
            "results/signal/transverse/" + B__HELK_Name + "/fixedParameterFromPreviuosFit.txt");
    map<string, double> *mapTest = testRead->read();

    *_myLogger << "KstHel_tr_signal: " << endl;
    a_kst_tr->setVal(mapTest->find("a")->second);
    a_kst_tr->setConstant();
    b_kst_tr->setVal(mapTest->find("b")->second);
    b_kst_tr->setConstant();
    c_kst_tr->setVal(mapTest->find("c")->second);
    c_kst_tr->setConstant();

    *_myLogger << "KstHel_tr_signal: a = " << a_kst_tr->getValV() << endl;
    *_myLogger << "KstHel_tr_signal: b = " << b_kst_tr->getValV() << endl;
    *_myLogger << "KstHel_tr_signal: c = " << c_kst_tr->getValV() << endl;
}

void KstHel_tr_signal::print() {
    a_kst_tr->Print();
    b_kst_tr->Print();
    c_kst_tr->Print();
}
