#ifndef KstHel_tr_signal_NEW
#define KstHel_tr_signal_NEW

#include "RooAddPdf.h"
#include "RooRealVar.h"

#include "../../../Observables.h"

#ifndef ROOT_TObject
#include "TObject.h"
#endif

class KstHel_tr_signal : public TObject{

  RooRealVar* a_kst_tr;
  RooRealVar* b_kst_tr;
  RooRealVar* c_kst_tr;

Observables* observable;
ofstream* _myLogger;

public:
  KstHel_tr_signal(Observables* observable_obj,ofstream* myLogger);
  RooAbsPdf* createPdfObservable();
  void print();
  void initParameterFix();

  ClassDef(KstHel_tr_signal,1);

};

#endif
