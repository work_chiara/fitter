#ifndef Rho0_M_signal_NEW
#define Rho0_M_signal_NEW

#include "RooAddPdf.h"
#include "RooRealVar.h"

#include "../../../Observables.h"

#ifndef ROOT_TObject
#include "TObject.h"
#endif

class Rho0_M_signal : public TObject{

RooRealVar* mean_rho_M;
RooRealVar* width_rho_M;
RooRealVar* _spin;
RooRealVar* _radius;
RooRealVar* _mass_a;
RooRealVar* _mass_b;

Observables* observable;
ofstream* _myLogger;

public:
  Rho0_M_signal(Observables* observable_obj,ofstream* myLogger);
  RooAbsPdf* createPdfObservable();
  void print();
  void initParameterFix();

  ClassDef(Rho0_M_signal,1);

};

#endif
