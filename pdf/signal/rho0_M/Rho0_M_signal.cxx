#include "Rho0_M_signal.h"
#include "RooGaussian.h"
#include "RooCBShape.h"
#include "RooPolynomial.h"
#include "RooBreitWigner.h"
#include <fstream>

#include "../../../ReadWriteInputFile.h"
#include "RooGounarisSakurai.h"

ClassImp(Rho0_M_signal)

Rho0_M_signal::Rho0_M_signal(Observables *observable_obj, ofstream *myLogger) : TObject() {

    observable = observable_obj;
    _myLogger = myLogger;

    ReadWriteInputFile *testRead = new ReadWriteInputFile("pdf/signal/rho0_M/inputFile_Rho0_M_signal.txt");
    map<string, double> *mapTest = testRead->read();

    double meanMin = mapTest->find("meanMin")->second;
    double meanMax = mapTest->find("meanMax")->second;
    double widthMin = mapTest->find("widthMin")->second;
    double widthMax = mapTest->find("widthMax")->second;
    double radiusMin = mapTest->find("radiusMin")->second;
    double radiusMax = mapTest->find("radiusMax")->second;

    double mean_initParameter = mapTest->find("mean_initParameter")->second;
    double sigma_initParameter = mapTest->find("sigma_initParameter")->second;

    mean_rho_M = new RooRealVar("mean_rho_M", "mean_rho_M", mean_initParameter, meanMin, meanMax);
    width_rho_M = new RooRealVar("width_rho_M", "width_rho_M", sigma_initParameter, widthMin, widthMax);

    double mean_fixedValue = mapTest->find("mean_fixedValue")->second;
    double sigma_fixedValue = mapTest->find("sigma_fixedValue")->second;

    if (mapTest->find("mean_setFixedValue")->second) {
        mean_rho_M->setVal(mean_fixedValue);
        mean_rho_M->setConstant();
    }
    if (mapTest->find("sigma_setFixedValue")->second) {
        width_rho_M->setVal(sigma_fixedValue);
        width_rho_M->setConstant();
    }

}

RooAbsPdf *Rho0_M_signal::createPdfObservable() {

    RooRealVar *rhoMass = observable->getObeservable(B__MR);

    RooAbsPdf *breitWigner = new RooBreitWigner("breitWigner_rhoMass_signal", "Breit-Wigner", *rhoMass, *mean_rho_M,
                                                *width_rho_M);
    return breitWigner;
}

void Rho0_M_signal::initParameterFix() {

    string B__MR_Name(B__MR);
    ReadWriteInputFile *testRead = new ReadWriteInputFile(
            "results/signal/" + B__MR_Name + "/fixedParameterFromPreviuosFit.txt");
    map<string, double> *mapTest = testRead->read();


    *_myLogger << "Rho0_M_signal: B__Mbc_corr parameter pdf (gaussian) init: " << endl;
    mean_rho_M->setVal(mapTest->find("mean")->second);
    mean_rho_M->setConstant();
    width_rho_M->setVal(mapTest->find("width")->second);
    width_rho_M->setConstant();

    *_myLogger << "Rho0_M_signal: mean = " << mean_rho_M->getValV() << endl;
    *_myLogger << "Rho0_M_signal: sigma = " << width_rho_M->getValV() << endl;

}

void Rho0_M_signal::print() {
    mean_rho_M->Print();
    width_rho_M->Print();
}
