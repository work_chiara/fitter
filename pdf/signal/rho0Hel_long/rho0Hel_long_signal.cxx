#include "rho0Hel_long_signal.h"
#include "RooGaussian.h"
#include "RooCBShape.h"
#include "RooPolynomial.h"
#include "RooProdPdf.h"
#include <RooGenericPdf.h>
#include <fstream>

#include "../../../ReadWriteInputFile.h"

ClassImp(rho0Hel_long_signal)

rho0Hel_long_signal::rho0Hel_long_signal(Observables *observable_obj, ofstream *myLogger) : TObject() {

    observable = observable_obj;
    _myLogger = myLogger;

    ReadWriteInputFile *testRead = new ReadWriteInputFile("pdf/signal/rho0Hel_long/inputFile_rho0Hel_long_signal.txt");
    map<string, double> *mapTest = testRead->read();

    double aMin = mapTest->find("aMin")->second;
    double aMax = mapTest->find("aMax")->second;
    double bMin = mapTest->find("bMin")->second;
    double bMax = mapTest->find("bMax")->second;
    double cMin = mapTest->find("cMin")->second;
    double cMax = mapTest->find("cMax")->second;

    double a_initParameter = mapTest->find("a_initParameter")->second;
    double b_initParameter = mapTest->find("b_initParameter")->second;
    double c_initParameter = mapTest->find("c_initParameter")->second;

    double a_fixedValue = mapTest->find("a_fixedValue")->second;
    double b_fixedValue = mapTest->find("b_fixedValue")->second;
    double c_fixedValue = mapTest->find("c_fixedValue")->second;


    a_rho_ln = new RooRealVar("a_rho_ln", "a_rho_ln", a_initParameter, aMin, aMax);
    b_rho_ln = new RooRealVar("b_rho_ln", "b_rho_ln", b_initParameter, bMin, bMax);
    c_rho_ln = new RooRealVar("c_rho_ln", "c_rho_ln", c_initParameter, cMin, cMax);

    if(mapTest->find("a_setValue")->second) {
        a_rho_ln->setVal(a_fixedValue);
        a_rho_ln->setConstant();
    }
    if(mapTest->find("b_setValue")->second) {
        b_rho_ln->setVal(b_fixedValue);
        b_rho_ln->setConstant();
    }
    if(mapTest->find("c_setValue")->second) {
        c_rho_ln->setVal(c_fixedValue);
        c_rho_ln->setConstant();
    }


}

RooAbsPdf *rho0Hel_long_signal::createPdfObservable() {

    RooRealVar *B__helR = observable->getObeservable(B__HELR);
    string B__HELR_Name(B__HELR);
    string ln_pow_function = B__HELR_Name + " * " + B__HELR_Name;


    RooAbsPdf *rho0_pow = new RooGenericPdf("rho0_pow_ln", "rho0_pow_ln",
                                            ln_pow_function.c_str(), RooArgList(*B__helR));
    RooArgList *polyParamList = new RooArgList(*a_rho_ln, *b_rho_ln, *c_rho_ln);
    RooAbsPdf *pol3 = new RooPolynomial("pol3_rho_ln", "pol3_rho_ln", *B__helR, *polyParamList);
    RooProdPdf *rho0Hel_long_pdf = new RooProdPdf("rho0Hel_long_pdf", "rho0Hel_long_pdf", RooArgList(*rho0_pow, *pol3));

    return rho0Hel_long_pdf;
}

void rho0Hel_long_signal::initParameterFix() {

    string B__HELR_Name(B__HELR);

    ReadWriteInputFile *testRead = new ReadWriteInputFile(
            "results/signal/longitudinal/" + B__HELR_Name + "/fixedParameterFromPreviuosFit.txt");
    map<string, double> *mapTest = testRead->read();

    *_myLogger << "rho0Hel_long_signal: parameter pdf init: " << endl;
    a_rho_ln->setVal(mapTest->find("a")->second);
    a_rho_ln->setConstant();
    b_rho_ln->setVal(mapTest->find("b")->second);
    b_rho_ln->setConstant();
    c_rho_ln->setVal(mapTest->find("c")->second);
    c_rho_ln->setConstant();


    *_myLogger << "rho0Hel_long_signal: a = " << a_rho_ln->getValV() << endl;
    *_myLogger << "rho0Hel_long_signal: b = " << b_rho_ln->getValV() << endl;
    *_myLogger << "rho0Hel_long_signal: c = " << c_rho_ln->getValV() << endl;

}

void rho0Hel_long_signal::print() {
    a_rho_ln->Print();
    b_rho_ln->Print();
    c_rho_ln->Print();
}
