#ifndef rho0Hel_signal_NEW
#define rho0Hel_signal_NEW

#include "RooAddPdf.h"
#include "RooRealVar.h"

#include "../../../Observables.h"

#ifndef ROOT_TObject
#include "TObject.h"
#endif

class rho0Hel_signal : public TObject{

  RooRealVar* a_long;
  RooRealVar* b_long;
  RooRealVar* c_long;

  RooRealVar* a_tr;
  RooRealVar* b_tr;
  RooRealVar* c_tr;

  RooRealVar *f_l;

Observables* observable;
ofstream* _myLogger;

public:
  rho0Hel_signal(Observables* observable_obj,ofstream* myLogger);
  RooAbsPdf* createPdfObservable();
  void print();
  void initParameterFix();

  ClassDef(rho0Hel_signal,1);

};

#endif
