#include "rho0Hel_signal.h"
#include "RooGaussian.h"
#include "RooCBShape.h"
#include "RooPolynomial.h"
#include <RooGenericPdf.h>
#include "RooProdPdf.h"
#include <fstream>

#include "../../../ReadWriteInputFile.h"

ClassImp(rho0Hel_signal)

rho0Hel_signal::rho0Hel_signal(Observables *observable_obj, ofstream *myLogger) : TObject() {

    observable = observable_obj;
    _myLogger = myLogger;

    ReadWriteInputFile *testRead = new ReadWriteInputFile("pdf/signal/rho0Hel/inputFile_rho0Hel_signal.txt");
    map<string, double> *mapTest = testRead->read();

    string B__HELR_Name(B__HELR);
    ReadWriteInputFile *testReadln = new ReadWriteInputFile(
            "results/signal/longitudinal/" + B__HELR_Name + "/fixedParameterFromPreviuosFit.txt");
    map<string, double> *mapTestln = testReadln->read();

    ReadWriteInputFile *testReadtr = new ReadWriteInputFile(
            "results/signal/transverse/" + B__HELR_Name + "/fixedParameterFromPreviuosFit.txt");
    map<string, double> *mapTesttr = testReadtr->read();


    a_long = new RooRealVar("a_long", "a_long",mapTestln->find("a_rho_ln")->second );
    b_long = new RooRealVar("b_long", "b_long", mapTestln->find("b_rho_ln")->second);
    c_long = new RooRealVar("c_long", "c_long", mapTestln->find("c_rho_ln")->second);

    a_tr = new RooRealVar("a_tr", "a_tr", mapTesttr->find("a_rho_tr")->second);
    b_tr = new RooRealVar("b_tr", "b_tr", mapTesttr->find("b_rho_tr")->second);
    c_tr = new RooRealVar("c_tr", "c_tr", mapTesttr->find("c_rho_tr")->second);

    f_l = new RooRealVar("f_l", "f_l", 0, 1);

}

RooAbsPdf *rho0Hel_signal::createPdfObservable() {

    RooRealVar *B__helR = observable->getObeservable(B__HELR);
    string B__HELR_Name(B__HELR);
    string ln_pow_function =  B__HELR_Name + " * " + B__HELR_Name;


    // longitudinal pdf
    RooAbsPdf *rho0_pow_ln = new RooGenericPdf("rho0_pow_ln", "rho0_pow_ln",
                                               ln_pow_function.c_str(), RooArgList(*B__helR));
    RooArgList *polyParamList_ln = new RooArgList(*a_long, *b_long, *c_long);
    RooAbsPdf *pol3_ln = new RooPolynomial("pol3_ln", "pol3_ln", *B__helR, *polyParamList_ln);
    RooProdPdf *rho0Hel_long_pdf = new RooProdPdf("rho0Hel_long_pdf", "rho0Hel_long_pdf",
                                                  RooArgList(*rho0_pow_ln, *pol3_ln));
    //transverse pdf
    string tr_pow_function = "1 - " + B__HELR_Name + " * " + B__HELR_Name;
    RooAbsPdf *rho0_pow_tr = new RooGenericPdf("rho0_pow_tr", "rho0_pow_tr",
                                               tr_pow_function.c_str(),
                                               RooArgList(*B__helR));
    RooArgList *polyParamList_tr = new RooArgList(*a_tr, *b_tr, *c_tr);

    RooAbsPdf *pol3_tr = new RooPolynomial("pol3_tr", "pol3_tr", *B__helR, *polyParamList_tr);

    RooProdPdf *rho0Hel_tr_pdf = new RooProdPdf("rho0Hel_tr_pdf", "rho0Hel_tr_pdf", RooArgList(*rho0_pow_tr, *pol3_tr));

    RooArgList *pdfCategoryList = new RooArgList(*rho0Hel_long_pdf, *rho0Hel_tr_pdf);
    RooAbsPdf *rho0Hel_pdf = new RooAddPdf("rho0Hel_pdf", "rho0Hel_pdf", *pdfCategoryList, *f_l);
    return rho0Hel_pdf;
}

void rho0Hel_signal::initParameterFix() {

    //ReadWriteInputFile* testRead = new ReadWriteInputFile("results/signal/rho0Hel/fixedParameterFromPreviuosFit.txt");
    //map<string, double>* mapTest = testRead->read();

    //*_myLogger << "rho0Hel_signal: B__Mbc_corr parameter pdf (gaussian) init: " << endl;
    cout << "rho0Hel_signal::initParameterFix" << endl;
    f_l->setVal(4.5450e-01); // CHANGE TODO
    f_l->setConstant();
    cout << "rho0Hel_signal::initParameterFix" << endl;
}

void rho0Hel_signal::print() {

}
