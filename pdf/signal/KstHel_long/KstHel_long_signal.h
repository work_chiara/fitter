#ifndef KstHel_long_signal_NEW
#define KstHel_long_signal_NEW

#include "RooAddPdf.h"
#include "RooRealVar.h"

#include "../../../Observables.h"

#ifndef ROOT_TObject
#include "TObject.h"
#endif

class KstHel_long_signal : public TObject{

RooRealVar* a_kst_ln;
RooRealVar* b_kst_ln;
RooRealVar* c_kst_ln;
RooRealVar* d_kst_ln;

Observables* observable;
ofstream* _myLogger;

public:
  KstHel_long_signal(Observables* observable_obj,ofstream* myLogger);
  RooAbsPdf* createPdfObservable();
  void print();
  void initParameterFix();

  ClassDef(KstHel_long_signal,1);

};

#endif
