#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooAddPdf.h"
#include "RooProdPdf.h"
#include "TCanvas.h"
#include "RooPlot.h"
#include <fstream>

#include "../deltaE/DeltaE_signal.h"
#include "../m_es/Mes_signal.h"
#include "../kstMass/kstMass_signal.h"
#include "../rho0_M/Rho0_M_signal.h"
#include "../KstHel_long/KstHel_long_signal.h"
#include "../KstHel_tr/KstHel_tr_signal.h"
#include "../KstHel/KstHel_signal.h"
#include "../rho0Hel_tr/rho0Hel_tr_signal.h"
#include "../rho0Hel_long/rho0Hel_long_signal.h"
#include "../rho0Hel/rho0Hel_signal.h"

#include "GlobalPdfSignal.h"

using namespace std;
using namespace RooFit;

ClassImp(GlobalPdfSignal)

GlobalPdfSignal::GlobalPdfSignal(bool initParamater, Observables *observable_obj, ofstream *myLogger) : TObject() {
    observable = observable_obj;
    initialParamater = initParamater;
    _myLogger = myLogger;
}

RooAbsPdf *GlobalPdfSignal::createGlobalPdf_long() {
    *_myLogger << "GlobalPdfSignal: global pdf signal build" << endl;

    RooAbsPdf *pdf_deltaE_signal = get_singlePDF_signal(B__DELTAE_CORR);
    RooAbsPdf *pdf_mes_signal = get_singlePDF_signal(B__MBC_CORR);
    RooAbsPdf *pdf_kstMass_signal = get_singlePDF_signal(B__MK);
    RooAbsPdf *pdf_Rho0_M_signal = get_singlePDF_signal(B__MR);
    RooAbsPdf *pdf_Ksthel_ln_signal = get_singlePDF_signal(B__HELK, LONGITUDINAL);
    RooAbsPdf *pdf_Rhohel_ln_signal = get_singlePDF_signal(B__HELR, LONGITUDINAL);

    RooProdPdf *global_pdf_signal = new RooProdPdf("global_pdf_signal_ln", "signal PDF ln",
                                                   RooArgList(*pdf_mes_signal, *pdf_deltaE_signal,
                                                              *pdf_kstMass_signal, *pdf_Rho0_M_signal,
                                                              *pdf_Ksthel_ln_signal, *pdf_Rhohel_ln_signal));

    *_myLogger << "GlobalPdfSignal: create global longitudinal signal pdf" << endl;
    return global_pdf_signal;
}

RooAbsPdf *GlobalPdfSignal::createGlobalPdf_transv() {

    RooAbsPdf *pdf_deltaE_signal = get_singlePDF_signal(B__DELTAE_CORR);
    RooAbsPdf *pdf_mes_signal = get_singlePDF_signal(B__MBC_CORR);
    RooAbsPdf *pdf_kstMass_signal = get_singlePDF_signal(B__MK);
    RooAbsPdf *pdf_Rho0_M_signal = get_singlePDF_signal(B__MR);
    RooAbsPdf *pdf_Ksthel_tr_signal = get_singlePDF_signal(B__HELK, TRANSVERSE);
    RooAbsPdf *pdf_Rhohel_tr_signal = get_singlePDF_signal(B__HELR, TRANSVERSE);


    RooProdPdf *global_pdf_signal = new RooProdPdf("global_pdf_signal_tr", "signal PDF tr",
                                                   RooArgList(*pdf_mes_signal, *pdf_deltaE_signal,
                                                              *pdf_kstMass_signal, *pdf_Rho0_M_signal,
                                                              *pdf_Ksthel_tr_signal, *pdf_Rhohel_tr_signal));

    *_myLogger << "GlobalPdfSignal: create global signal trasversal pdf" << endl;
    return global_pdf_signal;
}


RooAbsPdf *GlobalPdfSignal::get_singlePDF_signal(string observableName, string signalPolarization) {
    RooAbsPdf *pdf;

    if (observableName.compare(B__DELTAE_CORR) == 0) {
        DeltaE_signal *DeltaE_signal_obj = new DeltaE_signal(observable, _myLogger);
        if (initialParamater) {
            DeltaE_signal_obj->initParameterFix();
        }
        pdf = DeltaE_signal_obj->createPdfObservable();
    } else if (observableName.compare(B__MBC_CORR) == 0) {
        Mes_signal *mes_signal_obj = new Mes_signal(observable, _myLogger);
        if (initialParamater) {
            mes_signal_obj->initParameterFix();
        }
        pdf = mes_signal_obj->createPdfObservable();
    } else if (observableName.compare(B__MK) == 0) {
        kstMass_signal *kstMass_signal_obj = new kstMass_signal(observable, _myLogger);
        if (initialParamater) {
            kstMass_signal_obj->initParameterFix();
        }
        pdf = kstMass_signal_obj->createPdfObservable();
    } else if (observableName.compare(B__MR) == 0) {
        Rho0_M_signal *Rho0_M_signal_obj = new Rho0_M_signal(observable, _myLogger);
        if (initialParamater) {
            Rho0_M_signal_obj->initParameterFix();
        }
        pdf = Rho0_M_signal_obj->createPdfObservable();
    } else if (observableName.compare(B__HELK) == 0 && signalPolarization.compare(BOTH) == 0) {
        KstHel_signal *kstHel_signal_obj = new KstHel_signal(observable, _myLogger);
        if (initialParamater) {
            kstHel_signal_obj->initParameterFix();
        }
        pdf = kstHel_signal_obj->createPdfObservable();
    } else if (observableName.compare(B__HELK) == 0 && signalPolarization.compare(LONGITUDINAL) ==
                                                           0) {
        cout << "in global signal k ln " << endl;
        KstHel_long_signal *kstHel_long_signal_obj = new KstHel_long_signal(observable, _myLogger);
        if (initialParamater) {
            kstHel_long_signal_obj->initParameterFix();
        }
        pdf = kstHel_long_signal_obj->createPdfObservable();
    } else if (observableName.compare(B__HELK) == 0 && signalPolarization.compare(TRANSVERSE) == 0) {
        KstHel_tr_signal *kstHel_tr_signal_obj = new KstHel_tr_signal(observable, _myLogger);
        if (initialParamater) {
            kstHel_tr_signal_obj->initParameterFix();
        }
        pdf = kstHel_tr_signal_obj->createPdfObservable();
    } else if (observableName.compare(B__HELR) == 0 && signalPolarization.compare(BOTH) == 0) {
        rho0Hel_signal *rho0Hel_signal_obj = new rho0Hel_signal(observable, _myLogger);
        if (initialParamater) {
            rho0Hel_signal_obj->initParameterFix();
        }
        pdf = rho0Hel_signal_obj->createPdfObservable();
    } else if (observableName.compare(B__HELR) == 0 && signalPolarization.compare(LONGITUDINAL) == 0) {
        rho0Hel_long_signal *rho0Hel_long_signal_obj = new rho0Hel_long_signal(observable, _myLogger);
        if (initialParamater) {
            rho0Hel_long_signal_obj->initParameterFix();
        }
        pdf = rho0Hel_long_signal_obj->createPdfObservable();
    } else if (observableName.compare(B__HELR) == 0 && signalPolarization.compare(TRANSVERSE) == 0) {
        rho0Hel_tr_signal *rho0Hel_tr_signal_obj = new rho0Hel_tr_signal(observable, _myLogger);
        if (initialParamater) {
            rho0Hel_tr_signal_obj->initParameterFix();
        }
        pdf = rho0Hel_tr_signal_obj->createPdfObservable();
    } else {
        cout << "ERROR: pdf not FOUND!" << observableName << endl;

    }
    return pdf;
}
