
#ifndef POLARIZATION
#define POLARIZATION

static const char *const LONGITUDINAL = "longitudinal";

static const char *const TRANSVERSE = "transverse";

static const char *const BOTH = "both";
#endif

#ifndef Global_signal_NEW
#define Global_signal_NEW
#include "RooAddPdf.h"
#include "RooRealVar.h"
#include <fstream>

#include "../../../Observables.h"

#ifndef ROOT_TObject
#include "TObject.h"
#endif

class GlobalPdfSignal : public TObject {
    bool initialParamater = false;
    Observables *observable;
    ofstream *_myLogger;

public:
    GlobalPdfSignal(bool initParamater, Observables *observable_obj, ofstream* myLogger);
    RooAbsPdf *get_singlePDF_signal(string observableName,string signalPolarization = "");


    RooAbsPdf *createGlobalPdf_transv();
    RooAbsPdf *createGlobalPdf_long();


ClassDef(GlobalPdfSignal, 1);

};

#endif
