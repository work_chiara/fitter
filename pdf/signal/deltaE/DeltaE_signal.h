#ifndef DeltaE_signal_NEW
#define DeltaE_signal_NEW

#include "RooAddPdf.h"
#include "RooRealVar.h"

#include <string>
#include "../../../Observables.h"

#ifndef ROOT_TObject
#include "TObject.h"
#endif

using namespace std;

class DeltaE_signal : public TObject{

RooRealVar* mean1;
RooRealVar* sigma1;
RooRealVar* mean2;
RooRealVar* sigma2;
RooRealVar* f_g1;
RooAbsPdf * gaussian1;
RooAbsPdf * gaussian2;
Observables *observable;
    RooRealVar *B__deltaE_corr;
ofstream* _myLogger;

public:
  DeltaE_signal(Observables* observable_obj,ofstream* myLogger);
  RooAbsPdf* createPdfObservable();
  void print();
  void initParameterFix();
  RooAbsPdf *getSignalGaus1();
  RooAbsPdf *getSignalGaus2();

  ClassDef(DeltaE_signal,1);

};

#endif
