#ifndef kstMass_signal_NEW
#define kstMass_signal_NEW

#include "RooAddPdf.h"
#include "RooRealVar.h"

#include <string>
#include "../../../Observables.h"

#ifndef ROOT_TObject
#include "TObject.h"
#endif

using namespace std;

class kstMass_signal : public TObject{

RooRealVar* mean_kst_M;
RooRealVar* sigma_kst_M;
Observables *observable;
RooRealVar *B_kstMass;
ofstream* _myLogger;

public:
  kstMass_signal(Observables* observable_obj,ofstream* myLogger);
  RooAbsPdf* createPdfObservable();
  void print();
  void initParameterFix();

  ClassDef(kstMass_signal,1);

};

#endif
