#include "kstMass_signal.h"
#include "RooBreitWigner.h"
#include <fstream>

#include "../../../ReadWriteInputFile.h"

using namespace std;

ClassImp(kstMass_signal)

kstMass_signal::kstMass_signal(Observables *observable_obj, ofstream *myLogger) : TObject() {

    observable = observable_obj;
    _myLogger = myLogger;
    ReadWriteInputFile *readWriteInputFile = new ReadWriteInputFile("pdf/signal/kstMass/inputFile_kstMass_signal.txt");
    map<string, double> *mapTest = readWriteInputFile->read();

    B_kstMass = observable->getObeservable(B__MK);

    double meanMin = mapTest->find("meanMin")->second;
    double meanMax = mapTest->find("meanMax")->second;
    double sigmaMin = mapTest->find("sigmaMin")->second;
    double sigmaMax = mapTest->find("sigmaMax")->second;

    double mean_initParameter = mapTest->find("mean_initParameter")->second;
    double sigma_initParameter = mapTest->find("sigma_initParameter")->second;

    mean_kst_M = new RooRealVar("mean_kst_M", "mean of the Breit-Wigner kstMass variable",mean_initParameter, meanMin, meanMax);
    sigma_kst_M = new RooRealVar("sigma_kst_M", "width of the Breit-Wigner kstMass variable",sigma_initParameter, sigmaMin, sigmaMax);

    *_myLogger << "kstMass_signal: mean [" << meanMin << " , " << meanMax << "]" << endl;
    *_myLogger << "kstMass_signal: sigma [" << sigmaMin << " , " << sigmaMax << "]" << endl;

    double mean_fixedValue = mapTest->find("mean_fixedValue")->second;
    double sigma_fixedValue = mapTest->find("sigma_fixedValue")->second;

    if (mapTest->find("mean_setFixedValue")->second) {
        mean_kst_M->setVal(mean_fixedValue);
        mean_kst_M->setConstant();
    }
    if (mapTest->find("sigma_setFixedValue")->second) {
        sigma_kst_M->setVal(sigma_fixedValue);
        sigma_kst_M->setConstant();
    }

}

RooAbsPdf *kstMass_signal::createPdfObservable() {

    RooAbsPdf *breitWigner = new RooBreitWigner("breitWigner_kstM_signal", "Breit-Wigner", *B_kstMass, *mean_kst_M,
                                                *sigma_kst_M);
    return breitWigner;
}

void kstMass_signal::initParameterFix() {

    string B__MK_Name(B__MK);
    ReadWriteInputFile *testRead = new ReadWriteInputFile(
            "results/signal/" + B__MK_Name + "/fixedParameterFromPreviuosFit.txt");
    map<string, double> *mapTest = testRead->read();

    mean_kst_M->setVal(mapTest->find("mean")->second);
    mean_kst_M->setConstant();
    sigma_kst_M->setVal(mapTest->find("sigma")->second);
    sigma_kst_M->setConstant();

    *_myLogger << "kstMass_signal: mean = " << mean_kst_M->getValV() << endl;
    *_myLogger << "kstMass_signal: sigma = " << sigma_kst_M->getValV() << endl;

}


void kstMass_signal::print() {
    mean_kst_M->Print();
    sigma_kst_M->Print();
}
