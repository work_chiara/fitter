#include "rho0Hel_tr_signal.h"
#include "RooGaussian.h"
#include "RooCBShape.h"
#include "RooPolynomial.h"
#include "RooProdPdf.h"
#include <RooGenericPdf.h>
#include <fstream>

#include "../../../ReadWriteInputFile.h"

ClassImp(rho0Hel_tr_signal)

rho0Hel_tr_signal::rho0Hel_tr_signal(Observables *observable_obj, ofstream *myLogger) : TObject() {

    observable = observable_obj;
    _myLogger = myLogger;

    ReadWriteInputFile *testRead = new ReadWriteInputFile("pdf/signal/rho0Hel_tr/inputFile_rho0Hel_tr_signal.txt");
    map<string, double> *mapTest = testRead->read();

    double aMin = mapTest->find("aMin")->second;
    double aMax = mapTest->find("aMax")->second;
    double bMin = mapTest->find("bMin")->second;
    double bMax = mapTest->find("bMax")->second;
    double cMin = mapTest->find("cMin")->second;
    double cMax = mapTest->find("cMax")->second;

    double a_initParameter = mapTest->find("a_initParameter")->second;
    double b_initParameter = mapTest->find("b_initParameter")->second;
    double c_initParameter = mapTest->find("c_initParameter")->second;


    //a = new RooRealVar("a", "a", -1.2969e-06, aMin, aMax);
    //b = new RooRealVar("b", "b", 0, bMin, bMax);
    //c = new RooRealVar("c", "c", -4.7943e-07, cMin, cMax);

    a_rho_tr = new RooRealVar("a_rho_tr", "a_rho_tr", a_initParameter, aMin, aMax);
    b_rho_tr = new RooRealVar("b_rho_tr", "b_rho_tr", b_initParameter, bMin, bMax);
    c_rho_tr = new RooRealVar("c_rho_tr", "c_rho_tr", c_initParameter, cMin, cMax);

    double a_fixedValue = mapTest->find("a_fixedValue")->second;
    double b_fixedValue = mapTest->find("b_fixedValue")->second;
    double c_fixedValue = mapTest->find("c_fixedValue")->second;

    if (mapTest->find("a_setFixedValue")->second) {
        a_rho_tr->setVal(a_fixedValue);
        a_rho_tr->setConstant();
    }
    if (mapTest->find("b_setFixedValue")->second) {
        b_rho_tr->setVal(b_fixedValue);
        b_rho_tr->setConstant();
    }
    if (mapTest->find("c_setFixedValue")->second) {
        c_rho_tr->setVal(c_fixedValue);
        c_rho_tr->setConstant();
    }

}

RooAbsPdf *rho0Hel_tr_signal::createPdfObservable() {

    RooRealVar *B__helR = observable->getObeservable(B__HELR);
    string B__HELR_Name(B__HELR);
    string tr_pow_function = "1 - " + B__HELR_Name + " * " + B__HELR_Name;

    RooAbsPdf *rho0_pow = new RooGenericPdf("rho0_pow_tr", "rho0_pow_tr",
                                            tr_pow_function.c_str(),
                                            RooArgList(*B__helR));
    RooArgList *polyParamList = new RooArgList(*a_rho_tr, *b_rho_tr, *c_rho_tr);
    RooAbsPdf *pol3 = new RooPolynomial("pol3_tr_rho", "pol3_tr_rho", *B__helR, *polyParamList);
    RooProdPdf *rho0Hel_tr_pdf = new RooProdPdf("rho0Hel_tr_pdf", "rho0Hel_tr_pdf", RooArgList(*rho0_pow, *pol3));

    return rho0Hel_tr_pdf;
}

void rho0Hel_tr_signal::initParameterFix() {

    string B__HELR_Name(B__HELR);

    ReadWriteInputFile *testRead = new ReadWriteInputFile(
            "results/signal/transverse/" + B__HELR_Name + "/fixedParameterFromPreviuosFit.txt");
    map<string, double> *mapTest = testRead->read();

    *_myLogger << "rho0Hel_tr_signal: " << endl;
    a_rho_tr->setVal(mapTest->find("a")->second);
    a_rho_tr->setConstant();
    b_rho_tr->setVal(mapTest->find("b")->second);
    b_rho_tr->setConstant();
    c_rho_tr->setVal(mapTest->find("c")->second);
    c_rho_tr->setConstant();

    *_myLogger << "rho0Hel_tr_signal: a = " << a_rho_tr->getValV() << endl;
    *_myLogger << "rho0Hel_tr_signal: b = " << b_rho_tr->getValV() << endl;
    *_myLogger << "rho0Hel_tr_signal: c = " << c_rho_tr->getValV() << endl;

}

void rho0Hel_tr_signal::print() {
    a_rho_tr->Print();
    b_rho_tr->Print();
    c_rho_tr->Print();
    d_rho_tr->Print();
}
