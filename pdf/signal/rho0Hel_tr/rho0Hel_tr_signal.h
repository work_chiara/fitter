#ifndef rho0Hel_tr_signal_NEW
#define rho0Hel_tr_signal_NEW

#include "RooAddPdf.h"
#include "RooRealVar.h"

#include "../../../Observables.h"

#ifndef ROOT_TObject
#include "TObject.h"
#endif

class rho0Hel_tr_signal : public TObject{

  RooRealVar* a_rho_tr;
  RooRealVar* b_rho_tr;
  RooRealVar* c_rho_tr;
  RooRealVar* d_rho_tr;

Observables* observable;
ofstream* _myLogger;

public:
  rho0Hel_tr_signal(Observables* observable_obj,ofstream* myLogger);
  RooAbsPdf* createPdfObservable();
  void print();
  void initParameterFix();

  ClassDef(rho0Hel_tr_signal,1);

};

#endif
