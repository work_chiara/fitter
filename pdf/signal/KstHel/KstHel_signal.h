#ifndef KstHel_signal_NEW
#define KstHel_signal_NEW

#include "RooAddPdf.h"
#include "RooRealVar.h"

#include "../../../Observables.h"

#ifndef ROOT_TObject
#include "TObject.h"
#endif

class KstHel_signal : public TObject{

  RooRealVar* a_long;
  RooRealVar* b_long;
  RooRealVar* c_long;

  RooRealVar* a_tr;
  RooRealVar* b_tr;
  RooRealVar* c_tr;

  RooRealVar *f_l;

Observables* observable;
ofstream* _myLogger;

public:
  KstHel_signal(Observables* observable_obj,ofstream* myLogger);
  RooAbsPdf* createPdfObservable();
  void print();
  void initParameterFix();

  ClassDef(KstHel_signal,1);

};

#endif
