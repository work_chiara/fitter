#ifndef Mes_charm_bkg_NEW
#define Mes_charm_bkg_NEW

#include "RooAddPdf.h"
#include "RooRealVar.h"

#include "../../../Observables.h"

#ifndef ROOT_TObject
#include "TObject.h"
#endif

class Mes_charm_bkg : public TObject{

RooRealVar* m0;
RooRealVar* c;
Observables *observable;
ofstream* _myLogger;

public:
  Mes_charm_bkg(Observables* observable_obj,ofstream* myLogger);
  RooAbsPdf* createPdfObservable();
  void print();
  void initParameterFix();

  ClassDef(Mes_charm_bkg,1);

};

#endif
