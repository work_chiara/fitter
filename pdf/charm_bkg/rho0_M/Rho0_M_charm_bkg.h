#ifndef Rho0_M_charm_bkg_NEW
#define Rho0_M_charm_bkg_NEW

#include "RooAddPdf.h"
#include "RooRealVar.h"

#include "../../../Observables.h"

#ifndef ROOT_TObject
#include "TObject.h"
#endif

class Rho0_M_charm_bkg : public TObject{

RooRealVar* a0;
RooRealVar* a1;
RooRealVar* f;
RooRealVar* meanBG;
RooRealVar* widthBG;
RooRealVar* fGauss;
RooRealVar* meanGauss;
RooRealVar* sigmaGauss;
Observables *observable;
ofstream* _myLogger;

public:
  Rho0_M_charm_bkg(Observables* observable_obj,ofstream* myLogger);
  RooAbsPdf* createPdfObservable();
  void print();
  void initParameterFix();

  ClassDef(Rho0_M_charm_bkg,1);

};

#endif
