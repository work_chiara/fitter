#ifndef KstHel_charm_bkg_NEW
#define KstHel_charm_bkg_NEW

#include "RooAddPdf.h"
#include "RooRealVar.h"

#include "../../../Observables.h"

#ifndef ROOT_TObject
#include "TObject.h"
#endif

class KstHel_charm_bkg : public TObject{

  RooRealVar* a;
  RooRealVar* b;
  RooRealVar* c;

Observables* observable;
ofstream* _myLogger;

public:
  KstHel_charm_bkg(Observables* observable_obj,ofstream* myLogger);
  RooAbsPdf* createPdfObservable();
  void print();
  void initParameterFix();

  ClassDef(KstHel_charm_bkg,1);

};

#endif
