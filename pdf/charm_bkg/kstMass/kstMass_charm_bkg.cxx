#include "kstMass_charm_bkg.h"
#include "RooPolynomial.h"
#include "RooBreitWigner.h"
#include "RooArgList.h"
#include <iostream>
#include <fstream>
#include <string>
#include "../../../ReadWriteInputFile.h"

ClassImp(kstMass_charm_bkg)

kstMass_charm_bkg::kstMass_charm_bkg(Observables *observable_obj, ofstream *myLogger) : TObject() {

    observable = observable_obj;
    _myLogger = myLogger;
    ReadWriteInputFile *testRead = new ReadWriteInputFile("pdf/charm_bkg/kstMass/inputFile_kstMass_charm_bkg.txt");
    map<string, double> *mapTest = testRead->read();

    double a0Min = mapTest->find("a0Min")->second;
    double a0Max = mapTest->find("a0Max")->second;
    double fMin = mapTest->find("fMin")->second;
    double fMax = mapTest->find("fMax")->second;

    double a0_initParameter = mapTest->find("a0_initParameter")->second;
    double f_initParameter = mapTest->find("f_initParameter")->second;

    double a0_fixedValue = mapTest->find("a0_fixedValue")->second;
    double f_fixedValue = mapTest->find("f_fixedValue")->second;

    *_myLogger << "kstMass_charm_bkg: map size  =  " << mapTest->size() << endl;
    *_myLogger << "kstMass_charm_bkg: delta E charm_bkg" << endl;
    *_myLogger << "kstMass_charm_bkg: a0 [" << a0Min << " , " << a0Max << "]" << endl;
    *_myLogger << "kstMass_charm_bkg: f [" << fMin << " , " << fMax << "]" << endl;

    a0 = new RooRealVar("a0_Kst_mass_charm_bkg", "first parameter", a0_initParameter, a0Min, a0Max);
    f = new RooRealVar("f_Kst_mass_charm_bkg", "f", f_initParameter, fMin, fMax);

    if (mapTest->find("a0_setFixedValue")->second) {
        a0->setVal(a0_fixedValue);
        a0->setConstant();
    }
    if (mapTest->find("f_setFixedValue")->second) {
        f->setVal(f_fixedValue);
        f->setConstant();
    }

}

RooAbsPdf *kstMass_charm_bkg::createPdfObservable() {

    string B__MK_Name(B__MK);
    ReadWriteInputFile *readFromSignalResults = new ReadWriteInputFile(
            "results/signal/" + B__MK_Name + "/fixedParameterFromPreviuosFit.txt");
    map<string, double> *mapSignalResults = readFromSignalResults->read();

    double meanValue = mapSignalResults->find("mean_kst_M")->second;
    double sigmaValue = mapSignalResults->find("sigma_kst_M")->second;

    mean = new RooRealVar("mean_Kst_mass_charm_bkg", "mean Breit-Wigner", meanValue);
    mean->setConstant();
    sigma = new RooRealVar("sigma_Kst_mass_charm_bkg", "sigma Breit-Wigner", sigmaValue);
    sigma->setConstant();

    RooRealVar *kstMass = observable->getObeservable(B__MK);
    RooArgList *polyParamList = new RooArgList(*a0);
    // signal PDF for the x observable
    RooAbsPdf *pol1 = new RooPolynomial("pol2_Kst_mass_charm_bkg", "gaussian PDF for kstMass variable", *kstMass,
                                        *polyParamList);
    RooAbsPdf *breitWigner = new RooBreitWigner("breitWigner_Kst_mass_charm_bkg", "Breit-Wigner", *kstMass, *mean, *sigma);
    RooAbsPdf *polAndBW = new RooAddPdf("polAndBW_Kst_mass_charm_bkg", "1st order poly and BW from signal",
                                        RooArgList(*pol1, *breitWigner), *f);

    return polAndBW;

}

void kstMass_charm_bkg::initParameterFix() {

    string B__MK_Name(B__MK);
    ReadWriteInputFile *testRead = new ReadWriteInputFile(
            "results/charm_bkg/" + B__MK_Name + "/fixedParameterFromPreviuosFit.txt");
    map<string, double> *mapTest = testRead->read();

    *_myLogger << "kstMass_charm_bkg: kstMass parameter pdf (poly2) init: " << endl;
    a0->setVal(mapTest->find("a0")->second);
    a0->setConstant();
    f->setVal(mapTest->find("f")->second);
    f->setConstant();
    *_myLogger << "kstMass_charm_bkg: a0 = " << a0->getValV() << endl;
    *_myLogger << "kstMass_charm_bkg: f = " << f->getValV() << endl;

}

void kstMass_charm_bkg::print() {
    a0->Print();
    f->Print();
}
