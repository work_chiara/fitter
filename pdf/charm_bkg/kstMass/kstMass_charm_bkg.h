#ifndef kstMass_charm_bkg_NEW
#define kstMass_charm_bkg_NEW

#include "RooAddPdf.h"
#include "RooRealVar.h"

#include "../../../Observables.h"

#ifndef ROOT_TObject
#include "TObject.h"
#endif

class kstMass_charm_bkg : public TObject{

RooRealVar* a0;
RooRealVar* a1;
RooRealVar* f;
RooRealVar* mean;
RooRealVar* sigma;
Observables *observable;
ofstream* _myLogger;

public:
  kstMass_charm_bkg(Observables* observable_obj,ofstream* myLogger);
  RooAbsPdf* createPdfObservable();
  void print();
  void initParameterFix();

  ClassDef(kstMass_charm_bkg,1);

};

#endif
