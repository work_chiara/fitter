#ifndef rho0Hel_charm_bkg_NEW
#define rho0Hel_charm_bkg_NEW

#include "RooAddPdf.h"
#include "RooRealVar.h"

#include "../../../Observables.h"

#ifndef ROOT_TObject
#include "TObject.h"
#endif

class rho0Hel_charm_bkg : public TObject{

  RooRealVar* a;
  RooRealVar* b;
  RooRealVar* c;

Observables* observable;
ofstream* _myLogger;

public:
  rho0Hel_charm_bkg(Observables* observable_obj,ofstream* myLogger);
  RooAbsPdf* createPdfObservable();
  void print();
  void initParameterFix();

  ClassDef(rho0Hel_charm_bkg,1);

};

#endif
