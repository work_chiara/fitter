#ifndef Global_charm_bkg_NEW
#define Global_charm_bkg_NEW

#include "RooAddPdf.h"
#include "RooRealVar.h"
#include <fstream>

#include "../deltaE/DeltaE_charm_bkg.h"
#include "../m_es/Mes_charm_bkg.h"
#include "../kstMass/kstMass_charm_bkg.h"
#include "../rho0_M/Rho0_M_charm_bkg.h"
#include "../../../Observables.h"

#ifndef ROOT_TObject
#include "TObject.h"
#endif

class GlobalPdfCharmBkg : public TObject {
    bool initialParamater = false;
    Observables *observable;
    ofstream* _myLogger;

public:

    GlobalPdfCharmBkg(bool initParamater,Observables* observable_obj,ofstream* myLogger);
    RooAbsPdf *get_singlePDF_charm_bkg(string observableName);

    RooAbsPdf *createGlobalPdf();


ClassDef(GlobalPdfCharmBkg, 1);

};

#endif
