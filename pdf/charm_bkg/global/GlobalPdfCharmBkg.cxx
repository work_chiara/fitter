#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooAddPdf.h"
#include "RooProdPdf.h"
#include "TCanvas.h"
#include "RooPlot.h"
#include <fstream>

#include "../deltaE/DeltaE_charm_bkg.h"
#include "../m_es/Mes_charm_bkg.h"
#include "../kstMass/kstMass_charm_bkg.h"
#include "../rho0_M/Rho0_M_charm_bkg.h"
#include "../KstHel/KstHel_charm_bkg.h"
#include "../rho0Hel/rho0Hel_charm_bkg.h"
#include "GlobalPdfCharmBkg.h"


using namespace std;
using namespace RooFit;

ClassImp(GlobalPdfCharmBkg)


GlobalPdfCharmBkg::GlobalPdfCharmBkg(bool initParamater, Observables *observable_obj,ofstream* myLogger) : TObject() {
    initialParamater = initParamater;
    observable = observable_obj;
    _myLogger = myLogger;
}

RooAbsPdf *GlobalPdfCharmBkg::createGlobalPdf() {

    RooAbsPdf *pdf_deltaE_charm_bkg = get_singlePDF_charm_bkg(B__DELTAE_CORR);
    RooAbsPdf *pdf_mes_charm_bkg = get_singlePDF_charm_bkg(B__MBC_CORR);
    RooAbsPdf *pdf_kstMass_charm_bkg = get_singlePDF_charm_bkg(B__MK);
    RooAbsPdf *pdf_Rho0_M_charm_bkg = get_singlePDF_charm_bkg(B__MR);
    RooAbsPdf *pdf_Kst_hel_charm_bkg = get_singlePDF_charm_bkg(B__HELK);
    RooAbsPdf *pdf_Rho0_hel_charm_bkg = get_singlePDF_charm_bkg(B__HELR);

    RooProdPdf *global_pdf_charm_bkg = new RooProdPdf("global_pdf_charm_bkg_6var", "background 6 PDF",RooArgList(*pdf_deltaE_charm_bkg,*pdf_mes_charm_bkg,*pdf_kstMass_charm_bkg,*pdf_Rho0_M_charm_bkg,*pdf_Kst_hel_charm_bkg,*pdf_Rho0_hel_charm_bkg));


    return global_pdf_charm_bkg;
}

RooAbsPdf *GlobalPdfCharmBkg::get_singlePDF_charm_bkg(string observableName) {
    RooAbsPdf *pdf_observable_charm_bkg;

    if (observableName.compare(B__DELTAE_CORR) == 0) {
        DeltaE_charm_bkg * pcharm_bkg = new DeltaE_charm_bkg(observable,_myLogger);
        if (initialParamater) {
            pcharm_bkg->initParameterFix();
        }
        pdf_observable_charm_bkg = pcharm_bkg->createPdfObservable();
    } else if (observableName.compare(B__MBC_CORR) == 0) {
        Mes_charm_bkg *Mes_charm_bkg_obj = new Mes_charm_bkg(observable,_myLogger);

        if (initialParamater) {
            Mes_charm_bkg_obj->initParameterFix();
        }
        pdf_observable_charm_bkg = Mes_charm_bkg_obj->createPdfObservable();
    } else if (observableName.compare(B__MK) == 0) {
        kstMass_charm_bkg *kstMass_charm_bkg_obj = new kstMass_charm_bkg(observable,_myLogger);

        if (initialParamater) {
            kstMass_charm_bkg_obj->initParameterFix();
        }
        pdf_observable_charm_bkg = kstMass_charm_bkg_obj->createPdfObservable();
    } else if (observableName.compare(B__MR) == 0) {
        Rho0_M_charm_bkg *B_rho0_M_charm_bkg_obj = new Rho0_M_charm_bkg(observable,_myLogger);

        if (initialParamater) {
            B_rho0_M_charm_bkg_obj->initParameterFix();
        }
        pdf_observable_charm_bkg = B_rho0_M_charm_bkg_obj->createPdfObservable();
    } else if (observableName.compare(B__HELK) == 0) {
        KstHel_charm_bkg *KstHel_charm_bkg_obj = new KstHel_charm_bkg(observable,_myLogger);
        if (initialParamater) {
            KstHel_charm_bkg_obj->initParameterFix();
        }
        pdf_observable_charm_bkg = KstHel_charm_bkg_obj->createPdfObservable();
    } else if (observableName.compare(B__HELR) == 0) {
        rho0Hel_charm_bkg *rho0Hel_charm_bkg_obj = new rho0Hel_charm_bkg(observable,_myLogger);
        if (initialParamater) {
            rho0Hel_charm_bkg_obj->initParameterFix();
        }
        pdf_observable_charm_bkg = rho0Hel_charm_bkg_obj->createPdfObservable();
    }

    return pdf_observable_charm_bkg;
}
