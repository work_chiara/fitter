#include <cstdlib>
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "TCanvas.h"
#include "RooPlot.h"
#include "RooMCStudy.h"
#include "RooFitResult.h"
#include "TApplication.h"
#include "RooWorkspace.h"
#include "TFile.h"
#include <fstream>
#include <sstream>
#include "TSystem.h"
#include "RooHist.h"

void extractMeanSigma(string file, string numSig, string numBkg) {

    TFile *f = new TFile(file.c_str(), "READ");
    TCanvas *c1 = (TCanvas *) f->Get("fsigHisto");
    h = (RooHist *) c1->GetPrimitive("h_fitParData_model_fixParam_sig_and_bkg");
    TFitResultPtr r = h->Fit("gaus", "S");
    r->Parameter(1);
    r->Parameter(2);

    ofstream in;
    //in.open("meanFractionsFile.txt",kTRUE);
    in.open("test.txt", kTRUE);
    in << numSig << " " << numBkg << " " << r->Parameter(1) << " " << r->Parameter(2) << endl;

}
