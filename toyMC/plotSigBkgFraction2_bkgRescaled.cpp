#include <cstdlib>
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "TCanvas.h"
#include "RooPlot.h"
#include "RooMCStudy.h"
#include "RooFitResult.h"
#include "TApplication.h"
#include "RooWorkspace.h"
#include "TFile.h"
#include <fstream>
#include <sstream>
#include "TSystem.h"
#include "RooHist.h"
#include "TH2.h"

void plotSigBkgFraction2_bkgRescaled(string file) {

    float numSig;
    float numBkg;
    float fractionMean;
    float fractionErr;

    vector<float> numSig_vec, numBkg_vec, fractionMean_vec, fractionErr_vec, efu_vec;
// ---------------- PAD5
    ifstream in5;

    in5.open(("../" + file).c_str());
    int npt = 0;
    TH2F *g = new TH2F("g", "g", 50, 10, 1010, 333, 15, 5010);
    while (in5 >> numSig >> numBkg >> fractionMean >> fractionErr) {
        cout << "numSig = " << numSig << "  numBkg = " << numBkg << "  fractionMean = " << fractionMean
             << "  fractionErr = " << fractionErr << "  efu = " << fractionErr / fractionMean << endl;
        numSig_vec.push_back(numSig);
        numBkg = numBkg * 0.3;
        numBkg_vec.push_back(numBkg);
        fractionMean_vec.push_back(fractionMean);
        fractionErr_vec.push_back(fractionErr);
        efu_vec.push_back(fractionErr / fractionMean);
        g->Fill(numSig, numBkg, fractionErr / fractionMean);
        //g->Fill(100,100,100);
        npt++;
    }
    in5.close();

    TCanvas *c1 = new TCanvas("c1", "c1", 0, 0, 1200, 700);
    c1->cd();
    //g->Draw("surf1");
    //g->SetFillColor(kBlue);
    g->SetMarkerStyle(20);
    g->SetMarkerSize(0.5);
    g->SetYTitle("Background yield");
    g->SetXTitle("Signal yield");
    g->SetZTitle("relative error sig fraction");
    g->GetXaxis()->SetTitleOffset(1.7);
    g->GetYaxis()->SetTitleOffset(1.7);
    g->SetMarkerColor(kBlue);
    gStyle->SetPalette(1);
    g->Draw("LEGO2");

}
