#ifndef ReadWriteInputFile_NEW
#define ReadWriteInputFile_NEW

#include <map>

#ifndef ROOT_TObject
#include "TObject.h"
#endif

using namespace std;

class ReadWriteInputFile : public TObject {

 string _filePath;


public:
  ReadWriteInputFile(string path);

    map<string, string> * readString();

    map<string, double> * read();

  void write(map<string, double> * parameterMap);

ClassDef(ReadWriteInputFile, 2);

};

#endif
