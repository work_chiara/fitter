# commands to compile:

## Create header dictionary:
```
rootcint -f dict.cxx -c *.h pdf/*/*/*.h

```
## Build
it creates the executable file fitter
```
g++ `root-config --cflags --glibs` -L $ROOTSYS/lib -L $ROOTSYS/include -lRooFitCore -lRooFit -o fitter fitter.C FITMODEL.cxx pdf/*/*/*.cxx  Observables.cxx ReadWriteInputFile.cxx dict.cxx
```
## to run the fitter
```
./fitter runConfigFileFinal.txt
```

The runConfigFileFinal.txt containes the following configurations:
- which fit do you perform (fit only on signal, uds background etc...)
- if 1D fit, which observable you want to fit [B__deltaE_corr B__Mbc_corr B__MK B__MR B__helK B__helR all]
- for signal and control channel you can choose the polarization [longitudinal, transverse, both]
- the root file to use

## STRUCTURE OF THE FITTER:

In pdf directory you have a dir for each category. Then for each category there is a dir for each variable.
Inside there is a class where the pdf is defined. All the initial parameters are written in a file in the same directory.

The dir global contains a class that takes all the pdf for one category and define a global pdf.

Finally in the class named FITTER the global PDFs are taken and combined together to have a final model or it gives them to the final step fitter.C that perform the fit.

If you want to perform a 1D fit leaving the PDF parameters floating you need to create in fitter.C the object FITMODEL with the parameter FALSE:
 FITMODEL *pdfGenerator = new FITMODEL(false, myLogger);

If you want to fix them and leave only the signal and ploarization fractions floating you need to use a bool TRUE:
 FITMODEL *pdfGenerator = new FITMODEL(true, myLogger);
